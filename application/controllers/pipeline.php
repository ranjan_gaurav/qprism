<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Task Pipeline</h2></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <form method="post" action="<?php echo base_url('rroleader/report'); ?>"
          <div class="row">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-3 col-sm-4 col-md-4">
                        <div class="form-group" id="data_decade_view1">
                            <div class="input-group date">
                                <span class="input-group-addon">Start Date</span><input type="text" class="form-control" value="06/08/2016">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-4 col-md-4">
                        <div class="form-group" id="data_decade_view1">
                            <div class="input-group date">
                                <span class="input-group-addon">End Date</span><input type="text" class="form-control" value="06/08/2016">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-8 col-sm-8 col-md-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-content-new table-sc">
						<div class="content demo-yx">
                                <div class="table-responsive18">
                                    <table class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Lead#</th>
                                                <th>CIF#</th>
                                                <th>Customer Name</th>
                                                <th>Opportunity</th>
                                                <th>Rationale</th>
                                                <th>Task Status</th>
                                                <th>Follow-up Date</th>
                                                <th>Due Date</th>
                                                <th>Report</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="gradeX up-sell">
                                                <td>
                                                    <div class="radio">
                                                        <input type="radio" name="lead" id="radio1" value="1" required>
                                                        <label for="radio1">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>8720</td>
                                                <td>2187568</td>
                                                <td><a href="<?php echo base_url('rroleader/profile/4'); ?>">Ispat Indo</a></td>
                                                <td class="center">Up-sell -  OD Facility</td>
                                                <td class="center">Client's credit needs exceed current credit limit - OD has consistently high utilization levels</td>
                                                <td>Accepted by CF</td>
                                                <td class="center">6/1/2016</td>
                                                <td class="center">N/A</td>
                                                <td class="center"><a data-toggle="modal" data-target="#myModal4" class="pdf-icon"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            <tr class="gradeC up-sell">
                                                <td>
                                                    <div class="radio">
                                                        <input type="radio" name="lead" id="radio2" value="2" required>
                                                        <label for="radio2">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>6919</td>
                                                <td>2187569</td>
                                                <td><a href="<?php echo base_url('rroleader/profile/5'); ?>">Papajaya Agung Hutama Karya</a></td>
                                                <td class="center">Cross-sell - Giro Lehbi</td>
                                                <td class="center">Clients has overdraft facility - offer an upgrade to Giro Lehbi for greater flexibility</td>
                                                <td>Application Preparation</td>
                                                <td class="center">6/1/2016</td>
                                                <td class="center">N/A</td>
                                                <td class="center"><a data-toggle="modal" data-target="#myModal5" class="pdf-icon"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            <tr class="gradeA 52cecc up-sell">
                                                <td>
                                                    <div class="radio">
                                                        <input type="radio" name="lead" id="radio4" value="4" required>
                                                        <label for="radio4">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>9921</td>
                                                <td>5271399</td>
                                                <td><a href="<?php echo base_url('rroleader/profile/7'); ?>">Aluminium Light Metal Indonesia</a></td>
                                                <td class="center">Cross-sell - Credit card</td>
                                                <td class="center">Clients with OD or term loan with clean payment track record should be offered a pre-approved credit card</td>
                                                <td>Interested</td>
                                                <td class="center">6/6/2016</td>
                                                <td class="center">N/A</td>
                                                <td class="center"><a data-toggle="modal" data-target="#myModal6" class="pdf-icon"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            <tr class="gradeC loan-maintance">
                                                <td>
                                                    <div class="radio">
                                                        <input type="radio" name="lead" id="radio5" value="5" required>
                                                        <label for="radio5">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>6957</td>
                                                <td>5271400</td>
                                                <td><a href="<?php echo base_url('rroleader/profile/8'); ?>">Jindal Stainless Steel</a></td>
                                                <td class="center">Loan payment reminder</td>
                                                <td class="center">Payment due in next 4 days</td>
                                                <td>Pending</td>
                                                <td class="center">6/6/2016</td>
                                                <td class="center">6/10/2016</td>
                                                <td class="center"><a data-toggle="modal" data-target="#myModal7" class="pdf-icon"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            <tr class="gradeC loan-maintance">
                                                <td><!--<input type="checkbox" class="i-checks">-->
                                                    <div class="radio">
                                                        <input type="radio" name="lead" id="radio3" value="3" required>
                                                        <label for="radio3">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>9787</td>
                                                <td>2838580</td>
                                                <td><a href="<?php echo base_url('rroleader/profile/6'); ?>">Hutama Karya </a></td>
                                                <td class="center">Loan payment reminder</td>
                                                <td class="center">Payment due in next 2 days</td>
                                                <td>Completed</td>
                                                <td class="center">N/A</td>
                                                <td class="center">6/8/2016</td>
                                                <td class="center"><a data-toggle="modal" data-target="#myModal8" class="pdf-icon"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
						</div>
                        <div class="information-col">
                            <div class="information-col-maintance">Loan Maintenance</div>
                            <div class="information-col-up-sell">Up-sell/Cross-sell opportunity</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-sm-4 col-md-4 padding-off-left">
                <div class="ibox float-e-margins">
                    <div class="ibox-title"><h5>Alerts</h5></div>
                    <div class="ibox-content-new cc-add">
					<div class="content demo-yx">
                     <div class="table-responsive18">
                                    <table class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>CIF</th>
                                                <th>Description</th>
                                                <th>Escalated to</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php /* foreach ($alerts as $alert) : ?>
                                              <tr class="gradeX">
                                              <td><?php echo $alert->type; ?></td>
                                              <td><?php echo $alert->description; ?></td>
                                              <td><?php echo $alert->escalated_to; ?></td>
                                              </tr>
                                              <?php endforeach; */ ?>
                                            <tr class="gradeX">
                                                <td>NA</td>
                                                <td>1mth fail to meet KPIs</td>
                                                <td>Team leader</td>
                                            </tr>
                                            <tr class="gradeX">
                                                <td>NA</td>
                                                <td>CF Approval rate &#x3C; avg.</td>
                                                <td>Team leader</td>
                                            </tr>
                                            <tr class="gradeX">
                                                <td>3439324</td>
                                                <td>Loan >30 DPD</td>
                                                <td>Team Leader</td>
                                            </tr>
                                            <tr class="gradeX">
                                                <td>5932343</td>
                                                <td>Failure to renew loan / overdraft</td>
                                                <td>Team leader</td>
                                            </tr>
                                            <tr class="gradeX">
                                                <td>9475323</td>
                                                <td>Fail to engage lead by first 2wks</td>
                                                <td>Team leader</td>
                                            </tr>
                                            <tr class="gradeX">
                                                <td>8282733</td>
                                                <td>Loan >90 DPD</td>
                                                <td>Area Manager</td>
                                            </tr>
                                        </tbody>
                                    </table>
                     </div>
                    </div>
					</div>
                </div>
            </div>
        </div>

        <div class="lead-btns2">
            <input type="hidden" name="back_url" value="<?php echo base_url('rroleader/pipeline'); ?>">
            <button type="button" class="btn btn-w-m btn-info"><i class="fa fa-chevron-left"></i> Go to Back</button>
            <button type="submit" class="btn btn-w-m btn-warning">Plan activity</a>
        </div>
    </form>
    <div class="clearfix">&nbsp;</div>


    <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Report</h4>
                </div>
                <div class="modal-body">
                    <p class="infor"><strong>5.15.2016</strong> <br>Customer is interested in credit OD limit by 300 Mn IDR.<br></p>
                    <div class="hr-line-dashed"></div>
                    <p class="infor"><strong>5.28.2016</strong> <br>Application Prepared and submit successfully to CF.<br></p>
                    
                </div>
            </div>

        </div>
    </div>
    <div class="modal inmodal" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Report</h4>
                </div>
                <div class="modal-body">
                    <p class="infor"><strong>5.23.2016</strong> <br>Explain to customer about Giro Lehbi and document needs to be prepared.<br></p>
                    <div class="hr-line-dashed"></div>
                    <p class="infor"><strong>5.29.2016</strong> <br>Application in progress. Client needs more time to obtain a critical document.<br></p>
                    
                </div>
            </div>

        </div>
    </div>
    <div class="modal inmodal" id="myModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Report</h4>
                </div>
                <div class="modal-body">
                    <p class="infor"><strong>6.4.2016</strong> <br>Customer expressed interested in register credit card. Next meeting to follow up on application preparation.<br></p>
                    
                    
                </div>
            </div>

        </div>
    </div>
    <div class="modal inmodal" id="myModal7" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Report</h4>
                </div>
                <div class="modal-body">
                    <p class="infor">None report recorded yet.<br></p>
                    
                </div>
            </div>

        </div>
    </div>
    <div class="modal inmodal" id="myModal8" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Report</h4>
                </div>
                <div class="modal-body">
                    <p class="infor"><strong>6.2.2016</strong> <br>Reminded customer about payment coming. Customer was well prepared to settle on time.<br></p>
                </div>
            </div>

        </div>
    </div>
</div>
