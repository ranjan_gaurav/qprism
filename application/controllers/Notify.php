<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notify extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        include_once './application/objects/Response.php';
        $this->load->model('Notify_service');
        $this->load->library('session');
       // date_default_timezone_set("Asia/Kolkata");
    }
    /**
     * @author: Anjani Kr. Gupta
     * @method: index
     * @Desc : default page
     * Date: 20th Sept 2016
     */
	public function index()
	{
		if(isset($_REQUEST['userId'])){
			
			$id = $_REQUEST['userId'];
			
		}else{
			$id = SENDER_ID;
		}
		$data['count'] = $this->Notify_service->GetCountFollowup($id);
		$data['CountNotify'] = $this->Notify_service->GetCountNotify($id);
		$data['follow'] = $this->Notify_service->getfollowupList($id);  //print_r($data['follow'] ); die();
		$data['followUp'] = $this->Notify_service->getRecords3($id);
		$data['notify'] = $this->getNotification($id);
		$data['followhistory'] = $this->Notify_service->Getallfollow();
		$this->load->view('home',$data);
	}
	

	public function addRecord($val)
	{
		//print_r($_GET("loanNo")); die();
		$id = SENDER_ID;
		$data['page'] = $val;
		$data['callcat'] = $this->Notify_service->GetCallCategories();
		$data['history'] = $this->Notify_service->GetallNotifications();
		$data['count'] = $this->Notify_service->GetCountFollowup($id);
		$data['CountNotify'] = $this->Notify_service->GetCountNotify($id);
		$data['loanManager'] = $this->Notify_service->getLoanManagers($id);
		$data['follow'] = $this->Notify_service->getfollowupList($id); //print_r($data['follow']); die();
		$data['notify'] = $this->getNotification($id);
		$data['followhistory'] = $this->Notify_service->Getallfollow();  //print_r($data['followhistory']); die();
		$this->load->view('add_records',$data);
	}
	/**
	 * @author: Anjani Kr. Gupta
	 * @method: saveNotification
	 * @Desc : save notification data and send notification
	 * Date: 20th Sept 2016
	 */
	public function saveRecords()
	{
		
		
		
		
		$response = array();
		if(isset($_POST['loan'])){
			$loan = $_POST['loan'];  //print_r($loan); die();
			$data['borrowerName'] = $loan['borrower'];
			$data['borrowerEmail'] = $loan['email'];
			$data['borrowerPhone'] = $loan['phone'];
			$data['loanNumber'] = $loan['loanNo'];
			$data['currentStage'] = $loan['currentStage'];
			$data['loanAmount'] = $loan['loanAmount'];
			$data['loanOfficer'] = $loan['loanOfficer'];
			$data['loanProcessor'] = $loan['loanProcessor'];
			$data['processingManager'] = $loan['processingManager'];
			$data['callType'] = implode(",",$loan['callType']);
			$data['callCategory'] = $loan['callCategory'];
			$data['callCategoryNotes'] = $loan['callCategoryNotes'];
			$data['action'] = $loan['action'];
			$data['comments'] = $loan['comments'];
			$data['addedById'] = SENDER_ID ;
			$data['addedByName'] = $loan['addedByName'];
			$data['followupDateTime'] = $loan['followUpDate'];
			$data['followupTime'] = $loan['followUpTime'];
			$data['notifyTo'] = $loan['notifyUser'];
			$data['notifyToId'] = $loan['notifyUserId'];
			$data['receiver'] = $loan['receiver'];
		}else{
			$loan = NULL;
		}
		
		if($loan!=NULL){
			$notifyService = new Notify_service();
			$result = $notifyService->saveRecords($data);
			if($data['action'] == 1)
			{
		    $this->session->set_flashdata('message', 'FollowUp Added Successfully.');
			}
			else 
			{
			$this->session->set_flashdata('message', 'Notification Added Successfully.');
			}
			
			
			$response['status'] = $result->getStatus();
			$response['msg'] = $result->getMsg();
			$response['data'] = $result->getObjArray();
		}else{
			$response['status'] = FALSE;
			$response['msg'] = "Missing parameters.";
			$response['data'] = NULL;
		}
		
		echo json_encode($response);
	}
	
	/**
	 * @author: Anjani Kr. Gupta
	 * @method: notificationHistory
	 * @Desc : get notification history
	 * Date: 20th Sept 2016
	 */
	public function searchLoan()
	{
		$response = array();
		
		if(isset($_GET['loanNo'])){
			$data['loanNo'] = $_GET['loanNo'];
		}else{
			$data['loanNo'] = NULL;
		}
		
		if(isset($_GET['borrowerName'])){
			$data['borrowerName'] = $_GET['borrowerName'];
		}else{
			$data['borrowerName'] = NULL;
		}
		
		if(isset($_GET['borrowerEmail'])){
			$data['borrowerEmail'] = $_GET['borrowerEmail'];
		}else{
			$data['borrowerEmail'] = NULL;
		}
		
		if(isset($_GET['borrowerPhone'])){
			$data['borrowerPhone'] = $_GET['borrowerPhone'];
		}else{
			$data['borrowerPhone'] = NULL;
		}
		
		if($data['loanNo']==NULL && $data['borrowerName']==NULL){
			$response['status'] = FALSE;
			$response['msg'] = "Missing parameters.";
			$response['data'] = NULL;
		}else{
			$notifyService = new Notify_service();
			$result = $notifyService->searchLoan($data);
			
			$response['status'] = $result->getStatus();
			$response['msg'] = $result->getMsg();
			$response['data'] = $result->getObjArray();
		}
		
		echo json_encode($response);
	}
	
/*
	 * display all notification data of a user
	 */
	public function getNotification($id = null){
		
	    $userId = $id;// echo $userId ; die();
		if(isset($_GET['userId'])){
			$userId = $_GET['userId'];
		}else{
			$userId = $userId;
		}
		if($userId!=NULL){
			$notifyService = new Notify_service();
			$result = $notifyService->getNotification($userId);
			
			$response['status'] = $result->getStatus();
			$response['msg'] = $result->getMsg();
			$response['data'] = $result->getObjArray();
		}else{
			$response['status'] = FALSE;
			$response['msg'] = "Missing parameters.";
			$response['data'] = NULL;
		}
		 if(isset($_GET['userId'])){
		echo json_encode($response);
		}
		
		else {
			return $response;
		} 
		
            //   return $response;		
	}
	
	
	
	public function edit_records($id)
	{
		
		
		
		
		$nid = SENDER_ID;
		$flag = $this->Notify_service->ChangeFlag($id);
		$data['count'] = $this->Notify_service->GetCountFollowup($nid);
		$data['CountNotify'] = $this->Notify_service->GetCountNotify($nid);
		$data['records']=$this->Notify_service->getRecords($id);
		$data['reply']=$this->Notify_service->getRecords2($id);
		$data['followUp'] = $this->Notify_service->getRecords3($id);//print_r($data['followUp']); die();
		$data['follow'] = $this->Notify_service->getfollowupList($nid);
		$data['history'] = $this->Notify_service->GetallNotifications();
		$data['followhistory'] = $this->Notify_service->Getallfollow();
		$data['lastdate'] = $this->Notify_service->GetLastDate($id);
		$nid = SENDER_ID;
		$data['notify'] = $this->getNotification($nid); //print_r($data['notify']); die();
		$data['id'] = $id;
		$data['nid'] = $nid;
		$this->load->view('edit_records',$data);
		
		
		
		
	}
	
	public function update_records()
	{
		
		$data = $this->input->post();  //print_r($data); die();
		unset($data['id']);
		unset($data['receiver']);
		unset($data['followid']);
		unset($data['Date']);
		unset($data['time']);
		unset($data['UpdateData']);
	    $id = $this->input->post('id');
	    $reciever = $this->input->post('receiver');
	    $dateTime['Date'] = $this->input->post('Date');
	    $dateTime['time'] = $this->input->post('time');
	    
	   // print_r($dateTime); die();
	    $update = $this->Notify_service->UpdateRecords($id,$data,$dateTime);
	    
	    $followid = $this->input->post('followid');// echo $followid; die();
	  //  $updateFlag = $this->Notify_service->updateNotify($id);
	    
	    $insert = $this->Notify_service->InsertNofity($data,$id,$reciever);
	    
	    if ($update) {
	    	$this->session->set_flashdata('message', 'Data updated Successfully.');
	    } else {
	    	$this->session->set_flashdata('error', 'Something Went Wrong');
	    }
	    
	  //  redirect('index.php/Notify/edit_records'.'/'."$id");
	    //die();
	   header ( 'location:' . base_url ('index.php/Notify/edit_records'.'/'."$id"));
	}
	
	
	/*
	 * To show data when open notification
	 */
	public function getNotificationRecord(){
		if(isset($_GET['recordId'])){
			$recordId = $_GET['recordId'];
		}else{
			$recordId = NULL;
		}
		if($recordId!=NULL){
			$notifyService = new Notify_service();
			$result = $notifyService->getNotificationRecord($recordId);
	
			$response['status'] = $result->getStatus();
			$response['msg'] = $result->getMsg();
			$response['data'] = $result->getObjArray();
		}else{
			$response['status'] = FALSE;
			$response['msg'] = "Missing parameters.";
			$response['data'] = NULL;
		}
	
		echo json_encode($response);
	}
	/*
	 * notification alert
	 */
	public function notificationAlert(){
		if(isset($_GET['userId'])){
			$userId = $_GET['userId'];
		}else{
			$userId = NULL;
		}
		if($userId!=NULL){
			$notifyService = new Notify_service();
			$result = $notifyService->notificationAlert($userId);
	
			$response['status'] = $result->getStatus();
			$response['msg'] = $result->getMsg();
			$response['data'] = $result->getObjArray();
		}else{
			$response['status'] = FALSE;
			$response['msg'] = "Missing parameters.";
			$response['data'] = NULL;
		}
	
		echo json_encode($response);
	}
	
	
		
	public function autoSuggestionLoanNo(){
		$response = array();
		if(isset($_REQUEST['term'])){
			$loanNo = $_GET['term'];
		}else{
			$loanNo = NULL;
		}
		
		
		if($loanNo!=NULL){
			$notifyService = new Notify_service();
			$result = $notifyService->getAllLoanNo($loanNo);
		}else{
			$result = "";
		}
		
		echo json_encode($result);
	}
	public function autoSuggestionBorrowerName(){
		$response = array();
		if(isset($_REQUEST['term'])){
			$name = $_GET['term'];
		}else{
			$name = NULL;
		}
		
		
		if($name!=NULL){
			$notifyService = new Notify_service();
			$result = $notifyService->getAllBorrower($name);
		}else{
			$result = "";
		}
		
		echo json_encode($result);
	}
	
	
	public function filterfollowup()
	{
		$q = intval($_POST['id']); //echo $q; die();
		$data['filter'] = $this->Notify_service->getfilterfollowupList($q);
		$this->load->view('followmodal',$data);
	}
	

	public function filterNotify()
	{
		$q = intval($_POST['id']); //echo $q; die();
		$data['filter'] = $this->Notify_service->getfilterNotifyList($q); //print_r($data['filters']); die();
		$this->load->view('Notifymodal',$data);
	}
	
	
	
}
