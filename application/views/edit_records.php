<?php error_reporting(0);?>
<!DOCTYPE html>
<!-- saved from url=(0031)#/home -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Quatrro | Home</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!--<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>-->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<!-- bootstrap wysihtml5 - text editor -->
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet"
	href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
<!-- Theme style -->
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url(); ?>assets/css/style.css">
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url(); ?>assets/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
	folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="<?php echo base_url(); ?>assets/css/_all-skins.min.css">
<link rel="stylesheet"
	href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css">
<style>
.home_icon {
	float: left;
	width: 50px;
	padding: 0px 0px 0px 10px;
	height: 50px;
}

.main-header .logo .logo-lg {
	width: 80px;
}

.main-header .logo {
	width: 180px;
}

.sidebar-mini .sidebar-collapse .main-header .logo {
	width: 0px;
	padding: 0px;
}

.btn {
	border-radius: 4px;
	padding: 4px 10px !important;
}
</style>
<style>
.flexsearch--wrapper {
	height: auto;
	overflow: hidden;
	background: transparent;
	margin: 0;
	position: static;
}

.flexsearch--form {
	overflow: hidden;
	position: relative;
}

.flexsearch--input-wrapper {
	/*padding: 0 66px 0 0; /* Right padding for submit button width */
	overflow: hidden;
}

.flexsearch--input {
	width: 100%;
}

table {
	margin-bottom: 10px !important;
}
/***********************
 * Configurable Styles *
 ***********************/
.flexsearch {
	/*padding: 0 25px 0 200px; /* Padding for other horizontal elements */
	
}

.flexsearch--input {
	-webkit-box-sizing: content-box;
	-moz-box-sizing: content-box;
	box-sizing: content-box;
	/*height: 60px;*/
	padding: 0 46px 0 10px;
	border-color: #888;
	/*border-radius: 35px; /* (height/2) + border-width */
	border-style: solid;
	border-width: 5px;
	margin-top: 15px;
	color: #333;
	font-family: 'Helvetica', sans-serif;
	/*font-size: 26px;*/
	-webkit-appearance: none;
	-moz-appearance: none;
}

.flexsearch--submit {
	position: absolute;
	right: 0;
	top: 22px;
	display: block;
	width: 90px;
	/*height: 60px;*/
	padding: 0;
	border: none;
	/*margin-top: 20px; /* margin-top + border-width */
	margin-right: 13px; /* border-width */
	background: transparent;
	color: #888;
	font-family: 'Helvetica', sans-serif;
	/*font-size: 40px;*/
	line-height: 60px;
}

.flexsearch--input:focus {
	outline: none;
	border-color: #333;
}

.flexsearch--input:focus.flexsearch--submit {
	color: #333;
}

.flexsearch--submit:hover {
	color: #333;
	cursor: pointer;
}

.-webkit-input-placeholder {
	color: #888;
}

input:-moz-placeholder {
	color: #888
}
</style>
<style>
.modal-dialog {
	width: 750px !important;
	margin: 22px auto;
}

.panel-default>.panel-heading {
	color: #000;
	background-color: #00c0ef;
	border-color: #00acd6;
}

.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th,
	.table>thead>tr>td, .table>thead>tr>th {
	padding: 3px !important;
	border: none !important;
}

.style1 {
	color: #FF0000
}

#autoUpdate {
	display: none;
}

#autoUpdate2 {
	display: none;
}

.search-error {
	font-size: 12px;
	color: #f00;
	font-style: italic;
	text-align: center;
}

#loanForm {
	
}

#notifyUser {
	display: none;
}
/* #followUpDate{
	display: none;
}
#followUpTime{

display: none;

} */
</style>
<!-- <script src="http://code.jquery.com/jquery-1.9.1.js"></script> -->
<script type="text/javascript">


/* $(document).ready(function(){
$('#checkbox1').change(function(){
if(this.checked)
$('#autoUpdate').fadeIn('slow');
else
$('#autoUpdate').fadeOut('slow');

});
}); */

</script>

<script type="text/javascript">


/* $(document).ready(function(){
$('#checkbox2').change(function(){
if(this.checked)
$('#autoUpdate2').fadeIn('slow');
else
$('#autoUpdate2').fadeOut('slow');

});
});
 */
</script>


</head>
<body class="skin-blue sidebar-mini">
<?php
if (isset ( $_POST ['snooze'] )) {
	// echo $_POST['snooze_text'];
}

if (isset ( $_POST ['open'] )) {
	// echo $_POST['open_text'];
}
?>
	<div class="wrapper">
		<header class="main-header">
			<!-- home -->
			<a href="Quatrro _ Home.html" class="home_icon"> <img
				src="<?php echo base_url(); ?>assets/images/home.png">
			</a>
			<!-- home -->
			<!-- Logo -->
			<a href="#/index.php" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
				<!-- <span class="logo-mini"><b>QMS</b></span> --> <!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>QPrism</b></span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#/home#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span></a>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li style="margin: 10px 30px;" class="dropdown-toggle">
							<div class="dropdown">
								<button type="button" class="btn btn btn-follow"
									style="font: bold !important;" data-toggle="modal"
									data-target="#myModal">
									<i class="fa fa-bell" aria-hidden="true"></i>&nbsp;Notification
									<span class="notify"><?php echo $CountNotify;?></span>
								</button>
								<button type="button" class="btn btn btn-follow"
									style="font: bold !important;" data-toggle="modal"
									data-target="#myModal2">
									Follow Up<span class="notify"><?php echo $count;?></span>
								</button>
							</div>
						</li>
						<li class="dropdown user user-menu"></li>
						<li class="divider"></li>
						<!-- Menu Footer-->
						<li><a href="#/update-employee?empId=1545" class=""><i
								class="fa fa-user"></i>Profile</a></li>
						<li class="divider"></li>
						<li><a href="#/reset-pwd" class=""><i class="fa fa-cog"
								aria-hidden="true"></i>Reset Password</a></li>
						<li class="divider"></li>
						<li><a href="#/logout" class=""><i class="fa fa-power-off"
								aria-hidden="true"></i>Sign out</a></li>
						</li>
					</ul>
					</li>
					</ul>
				</div>
			</nav>
			<!-- upload photo -->
			<div id="popup1" class="overlay">
				<div class="popup">
					<h2>Change Profile Photo</h2>
					<a class="close" href="#/home#">×</a>
					<div class="content">
						Browse Photo To Change Your Profile Photo:
						<div class="up_msg"></div>
						<form name="imgupload" id="imgupload" method="post"
							enctype="multipart/form-data">
							<div id="uploader">
								<input type="file" name="fileToUpload" id="pro_pic"
									class="file-pic"> <input type="hidden" name="pichide"
									value="uploadpic">
							</div>
							<div class="upfoot">
								<button type="submit" name="upload" id="upload" value="Upload"
									class="btn btn-default">Upload</button>
								<a href="#/home" class="btn btn-default">Cancel</a>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- upload photo -->
		</header>
		<!--<div style="padding:0px;">-->
		<aside class="main-sidebar">
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href="#"> <i class="fa fa-bars"></i> <span>
								Scorecard/Queue</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									0 Scorecard <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									3 Scorecard <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									3 Doc Queue <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									3 CD Queue <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i>
									Tracking Report <!-- Ye sub menu sab aa rha hai -->
							</a></li>
						</ul></li>
				</ul>
			</section>
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href="#"> <i class="fa fa-bars"></i> <span>
								Charts</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									0 Chart <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									0 Qscore Chart <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									1 Chart <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									3 Chart <!-- Ye sub menu sab aa rha hai -->
							</a></li>
						</ul></li>
				</ul>
			</section>
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href="#"> <i class="fa fa-bars"></i> <span>
								Dashboard</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									1 Dashboard <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i>
									Demoted Step 1 Loans <!-- Ye sub menu sab aa rha hai -->
							</a></li>
						</ul></li>
				</ul>
			</section>
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href="#"> <i class="fa fa-bars"></i> <span>
								Assignments/Workload</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Loan
									Assign <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Task
									Assign <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i>
									Workload <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									3 Workload <!-- Ye sub menu sab aa rha hai -->
							</a></li>
						</ul></li>
				</ul>
			</section>
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href="#"> <i class="fa fa-bars"></i> <span>
								VPA</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
							<li><a href="#/filevision"> <i class="fa fa-shield fa-rotate-270"></i>
									Virtual Dashboard <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									3 VPA <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i>
									Demoted Loans VPA <!-- Ye sub menu sab aa rha hai -->
							</a></li>
						</ul></li>
				</ul>
			</section>
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href="#"> <i class="fa fa-bars"></i> <span>
								Checklist Email</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									0 Mail <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Step
									3 Mail <!-- Ye sub menu sab aa rha hai -->
							</a></li>
						</ul></li>
				</ul>
			</section>
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href="#"> <i class="fa fa-bars"></i> <span>
								Reports/User Tracker</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i>
									Manager Report <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i>
									Report <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> User
									Administrator <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> User
									Tracker <!-- Ye sub menu sab aa rha hai -->
							</a></li>
						</ul></li>
				</ul>
			</section>
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href="#"> <i class="fa fa-bars"></i> <span>
								Misc</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i>
									Search <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#/employee-list"> <i
									class="fa fa-shield fa-rotate-270"></i> User List <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#/add-employee"> <i
									class="fa fa-shield fa-rotate-270"></i> Add User <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Loan
									in Queue <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> User
									Feedback <!-- Ye sub menu sab aa rha hai -->
							</a></li>
						</ul></li>
				</ul>
			</section>
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href="#/reset-pwd"> <i class="fa fa-bars"></i>
							<span> Password Reset</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
						</ul></li>
				</ul>
			</section>
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href="#"> <i class="fa fa-bars"></i> <span>
								House Keeping</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
							<li><a href="#/manage-role"> <i
									class="fa fa-shield fa-rotate-270"></i> Manage Role <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#/manage-menu-list"> <i
									class="fa fa-shield fa-rotate-270"></i> Manage Menu List <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#/add-menu"> <i class="fa fa-shield fa-rotate-270"></i>
									Add Menu <!-- Ye sub menu sab aa rha hai -->
							</a></li>
							<li><a href="#/add-role"> <i class="fa fa-shield fa-rotate-270"></i>
									Add Role <!-- Ye sub menu sab aa rha hai -->
							</a></li>
						</ul></li>
				</ul>
			</section>
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href="#/logout"> <i class="fa fa-bars"></i>
							<span> Logout</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
						</ul></li>
				</ul>
			</section>
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href=""> <i class="fa fa-bars"></i> <span>
								Graph</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
							<li><a href="#/chart"> <i class="fa fa-shield fa-rotate-270"></i>
									Chart <!-- Ye sub menu sab aa rha hai -->
							</a></li>
						</ul></li>
				</ul>
			</section>
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview"><a href="#"> <i class="fa fa-bars"></i> <span>
								Support</span> <!-- ye jo hai main menu hai-->
					</a>
						<ul class="treeview-menu">
							<li><a href="#"> <i class="fa fa-shield fa-rotate-270"></i> Quick
									Tips <!-- Ye sub menu sab aa rha hai -->
							</a></li>
						</ul></li>
				</ul>
			</section>
		</aside>
		<div class="content-wrapper" style="min-height: 778px;">
			<section class="content-header">
				<div class="row">
					<div class="col-md-12">
						<div class="">
							<form id="recordForm" method="post"
								action="<?php echo base_url();?>index.php/Notify/update_records">
								<div class="panel-group">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" href="#collapse12"><b>Notes
														Recorder</b></a>
											</h4>
										</div>
										<div id="collapse12" class="panel-collapse collapse in">
                
                   <?php if (@$error): ?>
                            <div class="alert">
												<button type="button" class="close" data-dismiss="alert">×</button>
                                <?php echo $error; ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('message')) { ?>
                            <!-- <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success ! </strong> 
                            </div>-->
                            <script>
								//alert('success')
							</script>
                        <?php } ?>
                  <div class="panel-body">
												<?php if ($this->session->flashdata('message')) { ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
        </div>
    <?php } ?>
												<div class="row search-fields">
													<div class="col-sm-10">
														<div class="row">
															<div class="col-sm-3">
																<div class="form-group">
																	<label>Loan No</label> <input type="text"
																		class="select category form-control" id="loanNo"
																		name="loanNumber" placeholder=""
																		value="<?php echo $records->loanNumber;?>" disabled>
																</div>
															</div>
															<div class="col-sm-3">
																<div class="form-group">
																	<label>Name</label> <input type="text"
																		class="select category form-control" id="borrowerName"
																		name="borrowerName" placeholder=""
																		value="<?php echo $records->borrowerName;?>" disabled>
																</div>
															</div>
															<div class="col-sm-3" style="display:none;">
																<div class="form-group">
																	<label>Email</label> <input type="text"
																		class="select category form-control"
																		id="borrowerEmail" name="borrowerEmail" placeholder=""
																		value="<?php echo $records->borrowerEmail;?>" disabled>
																</div>
															</div>
															<div class="col-sm-3" style="display:none;">
																<div class="form-group">
																	<label>Phone</label> <input type="text"
																		class="select category form-control"
																		id="borrowerPhone" name="borrowerPhone" placeholder=""
																		value="<?php echo $records->borrowerPhone;?>" disabled>
																</div>
															</div>
														</div>
													</div>
													<div class="col-sm-2 text-right">
														<button class="btn btn-primary" id="btn-search"
															data-loading-text="Loading..." style="display: none;">Search</button>
													</div>
												</div>
												<div id="loanForm">
													<div class="thead-inverse">
														<div class="row col-5">
															<div class="col-sm-4">
																<div class="form-group">
																	<label>Loan Amount</label> <input type="text"
																		class="select category form-control" id="loanAmount"
																		name="loanAmount" placeholder="" disabled="disabled"
																		value="<?php echo $records->loanAmount;?>">
																</div>
															</div>
															<div class="col-sm-4">
																<div class="form-group">
																	<label>Current Stage</label> <input type="text"
																		class="select category form-control" id="currentStage"
																		name="currentStage" disabled="disabled" placeholder=""
																		value="<?php echo $records->currentStage;?>">
																</div>
															</div>
															<div class="col-sm-4">
																<div class="form-group">
																	<label>Loan Officer</label> <input type="text"
																		class="select category form-control" id="loanOfficer"
																		name="loanOfficer" placeholder="" disabled="disabled"
																		value="<?php echo $records->loanOfficer;?>">
																</div>
															</div>
															<div class="col-sm-4">
																<div class="form-group">
																	<label>Loan Processor</label> <input type="text"
																		class="select category form-control"
																		id="loanProcessor" name="loanProcessor" placeholder=""
																		disabled="disabled"
																		value="<?php echo $records->loanProcessor;?>">
																</div>
															</div>
															<div class="col-sm-4">
																<div class="form-group">
																	<label>Processing Manager</label> <input type="text"
																		class="select category form-control"
																		id="processingManager" name="processingManager"
																		placeholder="" disabled="disabled"
																		value="<?php echo $records->processingManager;?>">
																</div>
															</div>
														</div>
													</div>
													<div class="thead-inverse">
														<div class="thead-title">Add Notes</div>
														<div class="row">
															<div class="col-sm-3">
																<div class="form-group">
																	<label style="display: block;">Call Type</label> <label
																		class="checkbox-inline"> <input type="checkbox"
																		id="inbound" name="CallType" value="Inbound"
																		<?php if(isset($_POST['snooze'])){ echo 'disabled';} ?>
																		<?php if($records->CallType =="Inbound"){ echo "checked";}?>
																		name="calltype"> Inbound
																	</label> <label class="checkbox-inline"> <input
																		type="checkbox" id="outbound" name="CallType"
																		value="Outbound"
																		<?php if(isset($_POST['snooze'])){ echo 'disabled';} ?>
																		<?php if($records->CallType =="Outbound"){ echo "checked";}?>
																		name="calltype"> Outbound
																	</label>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="form-group">
																	<label><span class="style1">*</span> Call Category</label>
																	<select class="custom-select form-control"
																		id="callCategory" name="CallCategory" 	<?php if(isset($_POST['snooze'])){ echo 'disabled';} ?>
																	     <option
																			selected value="-1">Select Category</option>
																		<option value="1"
																			<?php if($records->CallCategory == '1') echo 'selected';?>>One</option>
																		<option value="2"
																			<?php if($records->CallCategory == '2') echo 'selected';?>>Two</option>
																		<option value="3"
																			<?php if($records->CallCategory == '3') echo 'selected';?>>Three</option>
																	</select>
																</div>
															</div>
															<div class="col-sm-5">
																<div class="form-group">
																	<label>Notes</label>
																	<textarea class="select category form-control" rows="3"
																		id="callCategoryNotes" name="CallCategoryNotes"
																		<?php if(isset($_POST['snooze'])){ echo 'disabled';} ?>
																		style="width: 400px"><?php echo $records->CallCategoryNotes;?></textarea>
																</div>
															</div>
														</div>
													</div>
													<div class="thead-inverse">
														<div class="thead-title">Actionable</div>
														<div class="row">
															<div class="col-sm-5">
																<div class="form-check" id="actionable">
																	<label class="form-check-label"> <input
																		class="form-check-input" type="radio" name="action"
																		id="followUp" name="action" value="1"
																		<?php if($records->action =="1"){ echo "checked";}?>
																		<?php if($records->action =="2") { ?>
																		disabled="disabled" <?php }?> style="font: normal;">
																		Follow Up
																	</label> <label class="form-check-label"> <input
																		class="form-check-input" type="radio" name="action"
																		id="notify" value="2" name="action"
																		<?php if($records->action =="2"){ echo "checked";}?>
																		<?php if($records->action =="1") { ?>
																		disabled="disabled" <?php }?> style="font: normal;">
																		Notify
																	</label> <label class="form-check-label"> <input
																		class="form-check-input" type="radio" name="action"
																		id="noAction" value="3" name="action"
																		<?php if($records->action =="3"){ echo "checked";} if(isset($_POST['snooze'])){ echo 'disabled';} ?>
																		style="font: normal;"> No action required
																	</label>
																</div>
																<div id="notifyUsers">
                          <?php if($records->action =="2"):?>
                 <select class="custom-select form-control"
																		name="receiver" style="width: 60%" disabled>
																		<option selected value="-1">Select User</option>
																		<option value="1"
																			<?php if(RECEIVER_ID == 1) {echo 'selected';}?>>AMIT</option>
																		<option value="2"
																			<?php if(RECEIVER_ID == 2) {echo 'selected';}?>>Dee
																			Featherston</option>
																		<option value="3">Robbinson</option>
																	</select>
                            <?php endif;?>
                        </div>
																<div id="followUpDate"
																	<?php  if($records->action =="1") { ?>
																	style="display: block;"
																	<?php } if($records->action =="2") { ?>
																	style="display:none;" <?php }?>>
																	<div class="form-group">
																		<div class='input-group date' id='datetimepicker2'
																			style="width: 60%">
																			<input type= "text" name="Date" class="form-control" placeholder="<?php echo date('m-d-Y',strtotime($lastdate->followupDate)); ?>"/>
                              <span class="input-group-addon"> <span
																				class="glyphicon glyphicon-calendar"></span>
																			</span>
																		</div>
																	</div>
																</div>

																<div id="followUpTime"
																	<?php  if($records->action =="1") { ?>
																	style="display: block;"
																	<?php } if($records->action =="2"){?>
																	style="display:none;" <?php }?>>
																	<div class="form-group">
																		<div class='input-group date' id='datetimepicker3'
																			style="width: 60%;">
																			<input type='text' class="form-control" name="time"
																				placeholder="<?php echo date('H:i',strtotime($lastdate->followupDate)); ?>" /> <span class="input-group-addon">
																				<span class="glyphicon glyphicon-time"></span>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-sm-7">
                           <?php
																											if ($records->action == "2") :
																												?>
                     
                        <div class="form-group">

																	<div class="reply-section"
																		style="border: 1px solid #ccc; overflow: auto;">
                          
                       <?php
																												$i = 0;
																												foreach ( $reply as $rec ) :
																													// print_r($rec);
																													
																													$i ++;
																													if ($i % 2 == 0) {
																														?>
                               <div class="comments"
																			<?php  if($records->action =="1") { ?>
																			style="display: none;" <?php }?>>
                              <?php
																														
																														echo '<div><b>' . $rec->senderName . ':' . '</b></div>';
																														echo '<div>' . $rec->comments . '</div>';
																														echo '<div style="display:block; text-align: right; font-style:italic;font-size:11px;">' . date ( 'm-d-Y : H:i', strtotime ( $rec->creationDate ) ) . '</div>';
																														
																														?>
                          </div> 
                          <?php   }else { ?>
                             <div class="comments"
																			<?php  if($records->action =="1") { ?>
																			style="display: none;" <?php }?>>
                          <?php
																														
																														echo '<div><b>' . $rec->senderName . ':' . '</b></div>';
																														echo '<div>' . $rec->comments . '</div>';
																														echo '<div style="display:block; text-align: right; font-style:italic;font-size:11px;">' . date ( 'm-d-Y : H:i', strtotime ( $rec->creationDate ) ) . '</div>';
																														
																														?>
                          </div> 
                          <?php
																													}
																													?>
                         <?php endforeach;?>
                         
                          </div>
																</div>
                        <?php endif;?>
                        
                         <?php
																									if ($records->action == "1") :
																										?>
                        
                        <div class="form-group">

																	<div class="reply-section"
																		style="border: 1px solid #ccc; overflow: auto;">
                          
                       <?php
																										$i = 0;
																										foreach ( $followUp as $fol ) :
																											//print_r($fol);
																											
																											$i ++;
																											if ($i % 2 == 0) {
																												?>
                               <div class="comments"
																			<?php  if($records->action =="2") { ?>
																			style="display: none;" <?php }?>>
                              <?php
																												
																												echo '<div><b>' . $fol->senderName . '</b></div>';
																												echo '<div style="display:block;">' . $fol->comments . '</div>';
																												echo '<div style="display:block; text-align: right; font-style:italic;font-size:11px;">' . $fol->creationDate . '</div>';
																												
																												?>
                          </div> 
                          <?php   }else { ?>
                             <div class="comments"
																			<?php  if($records->action =="2") { ?>
																			style="display: none;" <?php }?>>
                          <?php
																												
																												echo '<div><b>' . $fol->senderName . '</b></div>';
																												echo '<div style="display:block;">' . $fol->comments . '</div>';
																												echo '<div style="display:block; text-align: right; font-style:italic;font-size:11px;">' . $fol->creationDate . '</div>';
																												// echo '<div>'.$fol->comments.'</div>';
																												
																												?>
                          </div> 
                          <?php
																											}
																											?>
                         <?php endforeach;?>
                         
                          </div>
																</div>
                        <?php endif;?>
                        
                        
                        
                        
                        
                        
                        <div class="form-group"
																	<?php  if($records->action =="1") { ?>
																	style="width: 50%;" <?php }?>>
																	<label><?php if($records->action =="2"){ echo 'Reply';} else{echo 'Comment';} ?> </label>
																	<textarea class="select category form-control" rows="3"
																		<?php if(isset($_POST['snooze'])){ echo 'disabled';} ?>
																		id="comment" name="comments"><?php //if($records->action =="1"){ echo $records->comments;}?></textarea>
																</div>
															</div>
														</div>

													</div>
													<div class="row">
														<div class="col-sm-12 text-right">
															<input type="hidden" class="select category" id="id"
																name="id" value="<?php echo $records->id;?>"></input> <input
																type="hidden" class="select category" id="id"
																name="followid"
																value="<?php foreach($follow as $value): echo $value['id']; endforeach;?>"></input>
															<input type="submit" class="btn btn-success"
																data-loading-text="Saving..." id="UpdateData" name="UpdateData" data-toggle="modal" data-target=""
																value="Save"></input>
															<button type="button" class="btn btn-danger" id="cancel">Cancel</button>
														</div>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
							</form>
							<div class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" href="#hostory"><b>Show History</b></a>
										</h4>
									</div>
									<div id="hostory" class="panel-collapse collapse">
                <?php
																
																if ($records->action == "1") {
																	?>
									<table class="table table-striped">
											<thead>
												<tr>
													<th>Date</th>
													<th>Time</th>
													<th>Loan Number</th>
													<th>Comments</th>


												</tr>
											</thead>
										<?php
																	foreach ( $followhistory as $fol ) {
																		?>
										
										



										<tbody>
												<tr>
													<td><?php  echo date('m-d-Y',strtotime($fol['creationDate']));?></td>
													<td><?php echo date("H:i",strtotime($fol['creationDate'])); ?></td>
													<td><?php echo $fol['loanNumber']; ?></td>
													<td><?php echo $fol['comments'];?></td>



													<th><a
														href="<?php //echo base_url('index.php/Notify/edit_records'). '/' . $res['recordId'];?>">
															<!-- <button type="button" class="btn btn-info" id="button1">Open</button> -->
													</a></th>
											
											</tbody>
          	
          	
          
         <?php }}//	endforeach; }?>
       
          	
         
        </table>	
        
       		<?php
									
									if ($records->action == "2") {
										?>
									
									<table class="table table-striped">
											<thead>
												<tr>
													<th>Date</th>
													<th>Time</th>
													<th>Loan Number</th>
													<th>Rrequest From</th>
													<th>Response From</th>
													<th>Comments</th>
													
												</tr>
											</thead>
									<?php
										foreach ( $history as $res ) {
											?>
									
                                       <tbody>
												<tr>
													<td><?php  echo date('m-d-Y',strtotime($res['creationDate']));?></td>
													<td><?php echo date("H:i",strtotime($res['creationDate'])); ?></td>
													<td><?php echo $res['loanNumber']; ?></td>
													<td><?php echo $res['senderName'];?></td>
													<td><?php echo $res['receiverName'];?></td>
													<td><?php echo $res['comments'];?></td>


													<?php
											/* $now = new DateTime ();
											
											$then = $res ['creationDate'];
											$then = new DateTime ( $then );
											
											$sinceThen = $then->diff ( $now );
											
											echo $sinceThen->d . ' days'; */
											
											?>
												
													<th><a
														href="<?php //echo base_url('index.php/Notify/edit_records'). '/' . $res['recordId'];?>">
															<!-- <button type="button" class="btn btn-info" id="button1">Open</button> -->
													</a></th>
											
											</tbody>
          	
          	
          
         <?php }}//	endforeach; }?>
       
          	
         
        </table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		
		</div>
		</section>
	</div>

	<footer class="main-footer">
		<div class="pull-right hidden-xs"></div>
		<strong>Copyright © <a href="http://quatrro.com/">Quatrro Mortgage
				Solutions</a>.
		</strong> All rights reserved.
	</footer>
	<div class="control-sidebar-bg" style="position: fixed; height: auto;"></div>

	<!-- Password strength -->
	</div>
	<div id="myModal" class="modal fade in" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" style="display: none;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Notification</h4>
				</div>
				<div class="modal-body" id="text123">
					<div class="panel-group custom-accord" id="accordion"
						role="tablist" aria-multiselectable="true">
       <?php
							$i = 0;
							$j = 0;
							foreach ( $notify ['data'] as $res ) {
								$i ++;
								$j ++;
								?>
        <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<div class="row">
										<div class="col-sm-3">
											<div class="show-head">
												<label>Request From</label>
												<p><?php echo $res['senderName'];?></p>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="show-head">
												<label>Response From</label>
												<p><?php echo $res['receiverName'];?></p>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="show-head">
												<label>Days Pending</label>
												<p><?php
								$now = new DateTime ();
								
								$then = $res ['creationDate'];
								$then = new DateTime ( $then );
								
								$sinceThen = $then->diff ( $now );
								
								echo $sinceThen->h . ' days';
								
								?></p>
											</div>
										</div>
									</div>
									<a class="collapsed" role="button" data-toggle="collapse"
										data-parent="#accordion" href="#collapse<?php echo $j;?>"
										aria-expanded="true" aria-controls="collapse<?php echo $j;?>"></a>
								</h4>
							</div>
							<div id="collapse<?php echo $j;?>"
								class="panel-collapse collapse" role="tabpanel"
								aria-labelledby="heading<?php echo $j;?>">
								<div class="panel-body">
									<div class="row">
										<div class="col-sm-4">
											<p>
												<b>Loan Number: </b><?php echo $res['loanNumber']; ?></p>
											<p>
												<b>Date: </b><?php  echo date('m-d-Y',strtotime($res['creationDate']));?></p>
											<p>
												<b>Time: </b><?php echo date("H:i",strtotime($res['creationDate']));?></p>
										</div>
										<div class="col-sm-7 pl">
											<div class="comment-wrap">                          
                          <?php
								$i = 0;
								foreach ( $res ['comments'] as $comm ) :
									$i ++;
									if ($i % 2 == 0) {
										?>
                               <div class="comments">
                              <?php
										
										echo '<p><b>' . $comm ['senderName'] . ':' . '</b>';
										echo '' . $comm ['comments'] . '</p>';
										
										?>
										<span class="time"><?php echo $comm['creationDate'];?></span>
                          </div> 
                          <?php   }else { ?>
                             <div class="comments">
                          <?php
										
										echo '<p><b>' . $comm ['senderName'] . ':' . '</b>';
										echo '' . $comm ['comments'] . '</p>';
										
										?>
										<span class="time"><?php echo $comm['creationDate'];?></span>
                          </div> 
                          <?php
									}
									?>
                         
                          <?php endforeach;?>
                          </div>
										</div>
										<div class="col-sm-1 pl">
											<a
												href="<?php echo base_url('index.php/Notify/edit_records'). '/' . $res['recordId'];?>">
												<button type="button" class="btn btn-info" id="button1">Open</button>
											</a>
										</div>
									</div>

								</div>
							</div>
						</div>
        <?php }// endforeach; }?>
  </div>

				</div>
				<div class="modal-footer form-inline">
					<a
						href="<?php echo base_url('index.php/Notify/addRecord').'/'.'notify';?>">
						<button type="button" class="btn btn-primary" style="float: left">
							Add New</button>
					</a> <a
						href="<?php echo base_url('index.php/Notify/addRecord').'/'.'notify'; ?>">
						<button type="button" class="btn btn-search" style="float: left">
							Search</button>
					</a> <select class="form-control search" style="float: left" onchange=changeProfileInformation2(this.value)>
						<option value = "0">All</option>
        	<option value = "1">Today</option>
        	<option value = "2">This Week</option>
        	<option value = "3">Next Week</option>
					</select>
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
						<input type="hidden" class="category" id="profile12" name="profile12"
							value="<?php echo base_url('/index.php/Notify/filterNotify');?>"></input>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div id="myModal2" class="modal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" style="display: none;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">
						<b>Follow Up</b>
					</h4>
				</div>
				<div class="modal-body">

					<div class="modal-body" id="txtHintssss">
						<div class="panel-group custom-accord" id="accordion"
							role="tablist" aria-multiselectable="true">
       <?php
							$i = 0;
							$j = 0;
							// print_r($follow);
							foreach ( $follow as $row ) {
								// print_r($follow);
								$i ++;
								$j ++;
								?>
        <div class="panel panel-default">
								   <div class="panel-heading" role="tab" id="headingOne" <?php  if((strtotime(date('Y-m-d H:i:s')) > strtotime($row['followupDate'])) && $row['comments'][0]['flag'] == 0){ echo "style='background:rgba(255, 0, 0, 0.38);'"; } ?>>
									<h4 class="panel-title">
										<div class="row">
											<div class="col-sm-3">
												<div class="show-head">
													<label>Loan Number</label>
													<p><?php echo $row['loanNumber']; ?></p>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="show-head">
													<label>Date</label>
													<p><?php  echo date('m-d-Y',strtotime($row['followupDate']));?></p>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="show-head">
													<label>Time</label>
													<p><?php echo date("H:i",strtotime($row['followupDate']));?></p>
												</div>
											</div>
										</div>
										<a class="collapsed" role="button" data-toggle="collapse"
											data-parent="#accordion" href="#collap<?php echo $j;?>"
											aria-expanded="true" aria-controls="collapse<?php echo $j;?>"></a>
									</h4>
								</div>
								<div id="collap<?php echo $j;?>" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="heading<?php echo $j;?>">
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-2">
												<label class="comm">Comments</label>
											</div>
											<div class="col-sm-7 pl">
												<div class="comment-wrap">                          
                          <?php
								$i = 0;
								foreach ( $row ['comments'] as $comm ) :
									
									// print_r($comm);
									$i ++;
									if ($i % 2 == 0) {
										?>
                               <div class="comments">
                              <?php
										
										echo '<p><b>' . $comm ['senderName'] . ':' . '</b>';
										echo '' . $comm ['comments'] . '</p>';
										
										?>
										<span class="time"><?php echo $comm['creationDate'];?></span>
                          </div> 
                          <?php   }else { ?>
                             <div class="comments">
                          <?php
										
										echo '<p><b>' . $comm ['senderName'] . ':' . '</b>';
										echo '' . $comm ['comments'] . '</p>';
										
										?>
										<span class="time"><?php echo $comm['creationDate'];?></span>
                          </div> 
                          <?php
									}
									?>
                         
                          <?php endforeach;?>
                          </div>
											</div>
											<div class="col-sm-3">
											 <div class="pl floatL mR5">
												<a
													href="<?php echo base_url('index.php/Notify/edit_records'). '/' . $row['id'];?>">
													<button type="button" class="btn btn-info" id="button1">Open</button>
												</a>
											 </div>
											 <div class="pl floatL mR5">
												<a
													href="<?php echo base_url('index.php/Notify/edit_records'). '/' . $row['id'];?>">
													<input type="hidden" name="snooze_text" value="<?php echo $row['id'].'_2'; ?>" />
													<button type="button" class="btn btn-info" id="button1">Snooze</button>
												</a>
											</div>
											</div>
										</div>

									</div>
								</div>
							</div>
        <?php }// endforeach; }?>
  </div>
					</div>
					<div class="modal-footer form-inline">
						<a
							href="<?php echo base_url('index.php/Notify/addRecord').'/'.'follow';?>">
							<button type="button" class="btn btn-primary" style="float: left">
								Add New</button>
						</a> <a
							href="<?php echo base_url('index.php/Notify/addRecord').'/'.'follow';?>">
							<button type="button" class="btn btn-search" style="float: left">
								Search</button>
						</a> <select class="form-control search" style="float: left" onchange=changeProfileInformation(this.value)>
							<option value="0">All</option>
							<option value="1">Today</option>
							<option value="2">Last Week</option>
							<option value="3">Next Week</option>
						</select>
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Close</button>
						<input type="hidden" class="select category" id="id" name="id"
							value="<?php echo $records->id;?>"></input>
							<input type="hidden" class="category" id="profile" name="profile"
							value="<?php echo base_url('/index.php/Notify/filterfollowup');?>"></input>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	</div>
	<!-- This div is for div class wrapper included in header.php -->
	<div id="window-resizer-tooltip" style="display: none;">
		<a href="#/home#" title="Edit settings"></a><span class="tooltipTitle">Window
			size: </span><span class="tooltipWidth" id="winWidth">1280</span> x <span
			class="tooltipHeight" id="winHeight">984</span><br> <span
			class="tooltipTitle">Viewport size: </span><span class="tooltipWidth"
			id="vpWidth">1280</span> x <span class="tooltipHeight" id="vpHeight">879</span>
	</div>

    <div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         <a href="<?php echo base_url('index.php/Notify');?>">
          <button type="button" class="close" >&times;</button> </a>
          <h4 class="modal-title">Success</h4>
        </div>
        <div class="modal-body">
          <p>Data Successfully Updated</p>
        </div>
        <div class="modal-footer">
        <a href="<?php echo base_url('index.php/Notify');?>">
          <button type="button" class="btn btn-default" >Close</button> </a>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

	<footer>
		<script type="text/javascript"
			src="<?php echo base_url(); ?>assets/js/jquery-2.2.0.min.js"></script>
		<script type="text/javascript" language="javascript"
			src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- For 3d graph-->
		<script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/highcharts-3d.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/exporting.js"></script>
		<!-- End of 3d graph-->
		<!-- For column graph in jqwidgets -->
		<script type="text/javascript"
			src="<?php echo base_url(); ?>assets/js/jqxcore.js"></script>
		<script type="text/javascript"
			src="<?php echo base_url(); ?>assets/js/jqxchart.js"></script>
		<script type="text/javascript"
			src="<?php echo base_url(); ?>assets/js/jqxdata.js"></script>
		<!-- End of column graph -->
		<!-- Password strength -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap-strength.js"></script>
		<script>		
		$('.newpwd').bootstrapStrength({
			slimBar:true,
			minLenght: 8,
			upperCase: 1,
			numbers: 1,
			specialchars: 1
		});
		$('.cnewpwd').bootstrapStrength({
			slimBar:true,
			minLenght: 8,
			upperCase: 1,
			numbers: 1,
			specialchars: 1
		});		
		</script>
		<script
			src="<?php echo base_url(); ?>assets/js/moment-with-locales.js"></script>
		<script
			src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/notify.js"></script>
	</footer>
</body>
</html>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
            });
        </script>
        
        
       <script>
       changeProfileInformation = function (str) {

           var reqesturl = $('#profile').val();
           var value = str;
   	  //alert(reqesturl);
           $.ajax({
               type: 'POST',
               url: reqesturl,
               data: {
                   id:value
               },
               success: function (res) {
                   $("#txtHintssss").html(res);
               },
               dataType: 'html'
           });
       }

       changeProfileInformation2 = function (str) {

           var reqesturl = $('#profile12').val();
           var value = str;
         //alert(reqesturl);
           $.ajax({
               type: 'POST',
               url: reqesturl,
               data: {
                   id:value
               },
               success: function (res) {
                   $("#text123").html(res);
               },
               dataType: 'html'
           });
       }

       
     
</script>

