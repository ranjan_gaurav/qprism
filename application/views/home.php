<?php error_reporting(0);?>
<!DOCTYPE html>
<!-- saved from url=(0031)#/home -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Quatrro | Home</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/script.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<!-- bootstrap wysihtml5 - text editor -->
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
<!-- Theme style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
	folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/_all-skins.min.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>

<script type="text/javascript">
$(document).ready(function(){
$('#button1').change(function(){
if(this.checked)
$('#autoUpdate').fadeIn('slow');
else
$('#autoUpdate').fadeOut('slow');

});
});

</script>
<style>

#autoUpdate{ display:none;}
#autoUpdate2{ display:none;}



.home_icon {
	float:left;
	width: 50px;
	padding:0px 0px 0px 5px;
	background-color: #FFF;
	height:50px;
}
.main-header .logo .logo-lg {
	width: 80px;
}
.main-header .logo {
	width: 180px;
}

.sidebar-mini .sidebar-collapse .main-header .logo {
	width:0px;
	padding:0px;
}

.btn {
    border-radius: 4px;
	padding:4px 10px !important;}
</style>

<style>
.flexsearch--wrapper {
	height: auto;
	overflow: hidden;
	background: transparent;
	margin: 0;
	position: static;
}

.flexsearch--form {
	overflow: hidden;
	position: relative;
}

.flexsearch--input-wrapper {
	/*padding: 0 66px 0 0; /* Right padding for submit button width */
	overflow: hidden;
}

.flexsearch--input {
	width: 100%;
}

/***********************
 * Configurable Styles *
 ***********************/
.flexsearch {
  /*padding: 0 25px 0 200px; /* Padding for other horizontal elements */ 
}

.flexsearch--input {
	-webkit-box-sizing: content-box;
	-moz-box-sizing: content-box;
	box-sizing: content-box;
	/*height: 60px;*/
	padding: 0 46px 0 10px;
	border-color: #888;
	/*border-radius: 35px; /* (height/2) + border-width */
	border-style: solid;
	border-width: 5px;
	margin-top: 15px;
	color: #333;
	font-family: 'Helvetica', sans-serif;
	/*font-size: 26px;*/
	-webkit-appearance: none;
	-moz-appearance: none;
}

.flexsearch--submit {
  position: absolute;
  right: 0;
  top: 22px;
  display: block;
  width: 90px;
  /*height: 60px;*/
  padding: 0;
  border: none;
  /*margin-top: 20px; /* margin-top + border-width */
  margin-right: 13px; /* border-width */
  background: transparent;
  color: #888;
  font-family: 'Helvetica', sans-serif;
  /*font-size: 40px;*/
  line-height: 60px;
}


.flexsearch--input:focus {
	outline: none;
	border-color: #333;
}

.flexsearch--input:focus.flexsearch--submit {
	color: #333;
}

.flexsearch--submit:hover {
	color: #333;
	cursor: pointer;
}

.-webkit-input-placeholder {
	color: #888;
}

input:-moz-placeholder {
	color: #888
}
</style>
<style>
 
.modal-dialog {
    width: 800px !important;
    margin: 22px auto;}
.panel-default>.panel-heading {
    color: #000;
    background-color:#00c0ef;
    border-color: #00acd6; 
}
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 3px !important; border:none!important;}
.style1 {color: #FF0000}
</style>
</head>
<body class="skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- home -->
    <a href="#/home" class="home_icon"> <img src="<?php echo base_url(); ?>assets/images/home.png"> </a>
    <!-- home -->
    <!-- Logo -->
    <a href="#/index.php" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <!-- <span class="logo-mini"><b>QMS</b></span> -->
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>QPrism</b></span> </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#/home#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span></a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li style="margin:10px 30px;" class="dropdown-toggle">
            <div class="dropdown">
              <button type="button" class="btn btn btn-follow" style="font:bold !important;" data-toggle="modal" data-target="#myModal"> <i class="fa fa-bell" aria-hidden="true"></i>&nbsp;Notification <span class="notify"><?php echo $CountNotify;?></span> </button>
              <button type="button" class="btn btn btn-follow" style="font:bold !important;" data-toggle="modal" data-target="#myModal2"> Follow Up <span class="notify"><?php echo $count;?></span></button>
            </div>
          </li>
          <li class="dropdown user user-menu"> </li>
          <li class="divider"></li>
          <!-- Menu Footer-->
          <li > <a href="#/update-employee?empId=1545" class=""><i class="fa fa-user"></i>Profile</a> </li>
          <li class="divider"></li>
          <li> <a href="#/reset-pwd" class=""><i class="fa fa-cog" aria-hidden="true"></i>Reset Password</a> </li>
          <li class="divider"></li>
          <li> <a href="#/logout" class=""><i class="fa fa-power-off" aria-hidden="true"></i>Sign out</a> </li>
          </li>
        </ul>
        </li>
        </ul>
      </div>
    </nav>
    <!-- upload photo -->
    <div id="popup1" class="overlay">
      <div class="popup">
        <h2>Change Profile Photo</h2>
        <a class="close" href="#/home#">×</a>
        <div class="content"> Browse Photo To Change Your Profile Photo:
          <div class="up_msg"></div>
          <form name="imgupload" id="imgupload" method="post" enctype="multipart/form-data">
            <div id="uploader">
              <input type="file" name="fileToUpload" id="pro_pic" class="file-pic">
              <input type="hidden" name="pichide" value="uploadpic">
            </div>
            <div class="upfoot">
              <button type="submit" name="upload" id="upload" value="Upload" class="btn btn-default">Upload</button>
              <a href="#/home" class="btn btn-default">Cancel</a> </div>
          </form>
        </div>
      </div>
    </div>
    <!-- upload photo -->
  </header>
  <!--<div style="padding:0px;">-->
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#/home#"> <i class="fa fa-bars"></i> <span> Scorecard/Queue</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 0 Scorecard
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 3 Scorecard
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 3 Doc Queue
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 3 CD Queue
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Tracking Report
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
          </ul>
        </li>
      </ul>
    </section>
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#/home#"> <i class="fa fa-bars"></i> <span> Charts</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 0 Chart
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 0 Qscore Chart
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 1 Chart
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 3 Chart
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
          </ul>
        </li>
      </ul>
    </section>
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#/home#"> <i class="fa fa-bars"></i> <span> Dashboard</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 1 Dashboard
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Demoted Step 1 Loans
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
          </ul>
        </li>
      </ul>
    </section>
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#/home#"> <i class="fa fa-bars"></i> <span> Assignments/Workload</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Loan Assign
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Task Assign
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Workload
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 3 Workload
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
          </ul>
        </li>
      </ul>
    </section>
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#/home#"> <i class="fa fa-bars"></i> <span> VPA</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
            <li> <a href="#/filevision"> <i class="fa fa-shield fa-rotate-270"></i> Virtual Dashboard
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 3 VPA
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Demoted Loans VPA
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
          </ul>
        </li>
      </ul>
    </section>
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#/home#"> <i class="fa fa-bars"></i> <span> Checklist Email</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 0 Mail
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Step 3 Mail
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
          </ul>
        </li>
      </ul>
    </section>
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#/home#"> <i class="fa fa-bars"></i> <span> Reports/User Tracker</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Manager Report
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Report
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> User Administrator
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> User Tracker
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
          </ul>
        </li>
      </ul>
    </section>
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#/home#"> <i class="fa fa-bars"></i> <span> Misc</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Search
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/employee-list"> <i class="fa fa-shield fa-rotate-270"></i> User List
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/add-employee"> <i class="fa fa-shield fa-rotate-270"></i> Add User
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Loan in Queue
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> User Feedback
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
          </ul>
        </li>
      </ul>
    </section>
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#/reset-pwd"> <i class="fa fa-bars"></i> <span> Password Reset</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
          </ul>
        </li>
      </ul>
    </section>
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#/home#"> <i class="fa fa-bars"></i> <span> House Keeping</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
            <li> <a href="#/manage-role"> <i class="fa fa-shield fa-rotate-270"></i> Manage Role
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/manage-menu-list"> <i class="fa fa-shield fa-rotate-270"></i> Manage Menu List
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/add-menu"> <i class="fa fa-shield fa-rotate-270"></i> Add Menu
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
            <li> <a href="#/add-role"> <i class="fa fa-shield fa-rotate-270"></i> Add Role
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
          </ul>
        </li>
      </ul>
    </section>
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#/logout"> <i class="fa fa-bars"></i> <span> Logout</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
          </ul>
        </li>
      </ul>
    </section>
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href=""> <i class="fa fa-bars"></i> <span> Graph</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
            <li> <a href="#/chart"> <i class="fa fa-shield fa-rotate-270"></i> Chart
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
          </ul>
        </li>
      </ul>
    </section>
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview"> <a href="#/home#"> <i class="fa fa-bars"></i> <span> Support</span>
          <!-- ye jo hai main menu hai-->
          </a>
          <ul class="treeview-menu">
            <li> <a href="#/home#"> <i class="fa fa-shield fa-rotate-270"></i> Quick Tips
              <!-- Ye sub menu sab aa rha hai -->
              </a> </li>
          </ul>
        </li>
      </ul>
    </section>
  </aside>
  <!--</div>-->
  <!--
<div class="panel panel-default">
	<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Collapsible Group 1</a>
	<div id="collapse1" class="panel-collapse collapse in">
		<div class="panel-body">Lorem ipsum</div>
	</div>
</div>


		<ul class="sidebar-menu">
			<li class="treeview">
				<a href="#"><i class="fa fa-pie-chart"></i><span>Graph</span><i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-dashboard"></i> Graph1</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Graph1</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Graph1</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Graph1</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Graph1</a></li>
				</ul>
			</li>
		</ul>


		<ul class="sidebar-menu">
			<li class="treeview">
				<a href="#"><i class="fa fa-pie-chart"></i><span>Graph</span><i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-dashboard"></i> Graph1</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Graph1</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Graph1</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Graph1</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Graph1</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"><i class="fa fa-dashboard"></i><span>Step</span><i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="step0scorecard.php"><i class="fa fa-dashboard"></i> Step 0 Scorecard</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Step 0 Chart</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Step 1 Chart</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Step 1 Dashboard</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Step 1 Review</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Step 3  CD Queue</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Step 3 CD Doc Queue</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Step 3 Scorecard</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Step 3 Workload</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Step 3 Checklist Mail</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Step 3 VPA</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"><i class="fa fa-pie-chart"></i><span>Report</span><i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-dashboard"></i> Manager Report</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Tracking Report</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Report</a></li>            
				</ul>
			</li>
			<li class="treeview">
				<a href="#"><i class="fa fa-pie-chart"></i><span>Loans</span><i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-dashboard"></i> Demoted Loans</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Loan Assign</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Loan In Queue</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Loan History</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"><i class="fa fa-pie-chart"></i><span>Others</span><i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-dashboard"></i> User Feedback</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Task Assign</a></li>
					<li><a href="employee-list"><i class="fa fa-dashboard"></i> Employee </a></li>
					<li><a href="add-employee"><i class="fa fa-dashboard"></i> Add Employee </a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Workload</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Add Checklists</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Create Library</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> User Administration</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> User Track</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Virtual Dashboard</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"><i class="fa fa-pie-chart"></i><span>Search</span><i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-dashboard"></i> Search</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Search Keyword</a></li>
				</ul>
			</li>
		</ul>
		-->
  <style>
.home_icon {
	float:left;
	width: 50px;
	padding:0px 0px 0px 5px;
	background-color: #FFF;
	height:50px;
}

.main-header .logo .logo-lg {
	width: 80px;
}

.main-header .logo {
	width: 180px;
}

.sidebar-mini.sidebar-collapse .main-header .logo {
	width:0px;
	padding:0px;
}
</style>
  <style>
.flexsearch--wrapper {
	height: auto;
	overflow: hidden;
	background: transparent;
	margin: 0;
	position: static;
}

.flexsearch--form {
	overflow: hidden;
	position: relative;
}

.flexsearch--input-wrapper {
	/*padding: 0 66px 0 0; /* Right padding for submit button width */
	overflow: hidden;
}

.flexsearch--input {
	width: 100%;
}

/***********************
 * Configurable Styles *
 ***********************/
.flexsearch {
  /*padding: 0 25px 0 200px; /* Padding for other horizontal elements */ 
}

.flexsearch--input {
	-webkit-box-sizing: content-box;
	-moz-box-sizing: content-box;
	box-sizing: content-box;
	/*height: 60px;*/
	padding: 0 46px 0 10px;
	border-color: #888;
	/*border-radius: 35px; /* (height/2) + border-width */
	border-style: solid;
	border-width: 5px;
	margin-top: 15px;
	color: #333;
	font-family: 'Helvetica', sans-serif;
	/*font-size: 26px;*/
	-webkit-appearance: none;
	-moz-appearance: none;
}

.flexsearch--submit {
  position: absolute;
  right: 0;
  top: 22px;
  display: block;
  width: 90px;
  /*height: 60px;*/
  padding: 0;
  border: none;
  /*margin-top: 20px; /* margin-top + border-width */
  margin-right: 13px; /* border-width */
  background: transparent;
  color: #888;
  font-family: 'Helvetica', sans-serif;
  /*font-size: 40px;*/
  line-height: 60px;
}


.flexsearch--input:focus {
	outline: none;
	border-color: #333;
}

.flexsearch--input:focus.flexsearch--submit {
	color: #333;
}

.flexsearch--submit:hover {
	color: #333;
	cursor: pointer;
}

::-webkit-input-placeholder {
	color: #888;
}

input:-moz-placeholder {
	color: #888
}
</style>
  <div class="content-wrapper" style="min-height: 778px;">
    <section class="content-header">
      <h1><small></small></h1>
    </section>
    <section class="content">
      <div class="row">
        <div style="float:right; margin-top: -42px;" class="col-md-4">
       
          <div style="margin-top:-17px;" class="flexsearch">
            <div class="flexsearch--wrapper">
              <form class="flexsearch--form" action="#/home#" method="post">
                <div class="flexsearch--input-wrapper">
                  <input class="flexsearch--input" type="search" placeholder="Loan No, Borrower Name">
                </div>
                <img class="flexsearch--submit" style="width:5%" src="<?php echo base_url(); ?>assets/images/magnifier.png">
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Loans Percentage In Each <b>Steps</b></h3>
            </div>
            <div class="box-body">
              <title>Highcharts Example</title>
              <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min(1).js"></script>
              <style>
#container-title{fill: #111111;
    color: #111111;
    font-size: 14px;
    font-weight: bold;
    font-family: Verdana;
}
</style>
              <script type="text/javascript">
        $(document).ready(function() {
            var options = {
                chart: {
                    type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            },
                    renderTo: 'container_3',
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: true
                },
                title: {
                    text: 'Step Wise Loan%',
                    style: {
                            color: '#fffff  ',
                             font: 'bold 14px "Trebuchet MS", Verdana, sans-serif'
                }
                },
                tooltip: {
                    formatter: function() {
                        // pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                         //return '{this.point.name}: <b>{this.percentage:.1f}%</b>';
                        return '<b>'+ this.point.name +'</b>: ' + this.percentage +' % Loans';
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        depth: 35,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            color: '#000000',
                            connectorColor: '#000000',
                            formatter: function() {
                                //return '<b>'+ this.point.name +'</b>: '+ this.percentage +' % Loans';
                                return '<b>'+ this.point.name +'</b>';
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'StepWise Data',
                      point: {
                    events: {
                click: function(event) 
                {

                if(this.x == 0)
                {
                    var zero = "filevision.php?token=0";
                    location.href = (zero);
                     event.preventDefault();
                }
                else if(this.x == 1)
                {
                    var one = "filevision.php?token=1";
                location.href = (one);
                 event.preventDefault();
                }
                else if(this.x == 2)
                {
                    var two = "filevision.php?token=2";
                location.href = (two);
                 event.preventDefault();
                }
                else if(this.x == 3)
                {
                    var three = "filevision.php?token=3";
                location.href = (three);
                 event.preventDefault();
                }
                else
                {

                }


                }
            }
                },
                    data: []
                    
                }]
            }
            
            $.getJSON("div/pie_data.php", function(json) {
                options.series[0].data = json;
                chart = new Highcharts.Chart(options);
            })
                
            
            
            
        });   
        </script>
              <div style="width:100%; float:left; height:400px;" id="container_3" data-highcharts-chart="1">
                <div class="highcharts-container" id="highcharts-2" style="position: relative; overflow: hidden; width: 294px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                  <svg version="1.1" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="294" height="400">
                    <desc>Created with Highcharts 4.2.4</desc>
                    <defs>
                      <clippath id="highcharts-3">
                        <rect x="0" y="0" width="294" height="400"></rect>
                      </clippath>
                      <lineargradient x1="0" y1="0" x2="0" y2="1" id="highcharts-14">
                        <stop offset="0" stop-color="#FFF" stop-opacity="1"></stop>
                        <stop offset="1" stop-color="#ACF" stop-opacity="1"></stop>
                      </lineargradient>
                      <lineargradient x1="0" y1="0" x2="0" y2="1" id="highcharts-15">
                        <stop offset="0" stop-color="#9BD" stop-opacity="1"></stop>
                        <stop offset="1" stop-color="#CDF" stop-opacity="1"></stop>
                      </lineargradient>
                    </defs>
                    <rect x="0" y="0" width="294" height="400" fill="#FFFFFF" class=" highcharts-background"></rect>
                    <g class="highcharts-series-group">
                      <path fill="#90ed7d" fill-opacity="0.25" d="M 0 0"></path>
                      <g class="highcharts-series highcharts-series-0 highcharts-tracker" transform="translate(10,41) scale(1 1)" style="cursor:pointer;">
                        <path fill="rgb(99,156,211)" d="M 143.4064167052544 124.00478761097318 L 143.4064167052544 142.56634061712006 L 131 190.56155300614688 L 131 172 Z" transform="translate(0,0)" visibility="visible"></path>
                        <path fill="rgb(222,138,67)" d="M 130.97247035418678 123.209635981484 L 130.97247035418678 141.77118898763086 L 131 190.56155300614688 L 131 172 Z" transform="translate(0,0)" visibility="hidden"></path>
                        <path fill="rgb(119,212,100)" d="M 197.2360207859425 158.32905360912795 L 197.2360207859425 176.89060661527483 L 131 190.56155300614688 L 131 172 Z" transform="translate(0,0)" visibility="visible"></path>
                        <path fill="rgb(222,138,67)" d="M 130.97316035413323 123.20963578925952 C 130.9729177533313 123.20963585598703 130.97271295498774 123.20963591304104 130.97247035418678 123.209635981484 L 130.97247035418678 141.77118898763086 C 130.97271295498774 141.77118891918792 130.9729177533313 141.7711888621339 130.97316035413323 141.7711887954064 Z" transform="translate(0,0)" visibility="hidden"></path>
                        <path fill="rgb(222,138,67)" d="M 131 172 C 131 172 131 172 131 172 L 131 190.56155300614688 C 131 190.56155300614688 131 190.56155300614688 131 190.56155300614688 Z" transform="translate(0,0)" visibility="hidden"></path>
                        <path fill="rgb(222,138,67)" d="M 130.97316035413323 123.20963578925952 L 130.97316035413323 141.7711887954064 L 131 190.56155300614688 L 131 172 Z" transform="translate(0,0)" visibility="hidden"></path>
                        <path fill="rgb(99,156,211)" d="M 130.98594654894504 123.20963311010813 L 130.98594654894504 141.771186116255 L 131 190.56155300614688 L 131 172 Z" transform="translate(0,0)" visibility="visible"></path>
                        <path fill="rgb(42,42,47)" d="M 197.2358274462527 158.32858525041695 L 197.2358274462527 176.89013825656383 L 131 190.56155300614688 L 131 172 Z" transform="translate(0,0)" visibility="visible"></path>
                        <path fill="rgb(99,156,211)" d="M 130.98594654894504 123.20963311010813 C 135.37677870423596 123.20900074814081 139.08714355173737 123.44653711119415 143.4064167052544 124.00478761097318 L 143.4064167052544 142.56634061712006 C 139.08714355173737 142.00809011734103 135.37677870423596 141.77055375428768 130.98594654894504 141.771186116255 Z" transform="translate(0,0)" visibility="visible"></path>
                        <path fill="rgb(99,156,211)" d="M 131 172 C 131 172 131 172 131 172 L 131 190.56155300614688 C 131 190.56155300614688 131 190.56155300614688 131 190.56155300614688 Z" transform="translate(0,0)" visibility="visible"></path>
                        <path fill="rgb(42,42,47)" d="M 143.40709545943696 124.00487533998677 L 143.40709545943696 142.56642834613365 L 131 190.56155300614688 L 131 172 Z" transform="translate(0,0)" visibility="visible"></path>
                        <path fill="rgb(119,212,100)" d="M 130.97247035418678 123.209635981484 L 130.97247035418678 141.77118898763086 L 131 190.56155300614688 L 131 172 Z" transform="translate(0,0)" visibility="visible"></path>
                        <path fill="rgb(42,42,47)" d="M 143.40709545943696 124.00487533998677 C 169.80117349241982 127.41640774081705 189.71747239919512 140.11596875468243 197.2358274462527 158.32858525041695 L 197.2358274462527 176.89013825656383 C 189.71747239919512 158.6775217608293 169.80117349241982 145.97796074696393 143.40709545943696 142.56642834613365 Z" transform="translate(0,0)" visibility="visible"></path>
                        <path fill="rgb(42,42,47)" d="M 131 172 C 131 172 131 172 131 172 L 131 190.56155300614688 C 131 190.56155300614688 131 190.56155300614688 131 190.56155300614688 Z" transform="translate(0,0)" visibility="visible"></path>
                        <path fill="rgb(119,212,100)" d="M 200 172 C 200 198.94617613083764 169.10764773832477 220.79036790187178 131 220.79036790187178 C 92.89235226167524 220.79036790187178 62.00000000000001 198.94617613083764 62 172 L 62 190.56155300614688 C 61.99999999999999 163.62222117175634 92.87450494504154 141.78193725232427 130.97247035418678 141.77118898763086 L 130.97247035418678 123.209635981484 C 92.87450494504154 123.2203842461774 61.99999999999999 145.06066816560946 62 172 L 62 190.56155300614688 C 62.00000000000001 217.50772913698452 92.89235226167524 239.35192090801866 131 239.35192090801866 C 169.10764773832477 239.35192090801866 200 217.50772913698452 200 190.56155300614688 Z" transform="translate(0,0)" visibility="visible"></path>
                        <path fill="rgb(119,212,100)" d="M 131 172 C 131 172 131 172 131 172 C 131 172 131 172 131 172 C 131 172 131 172 131 172 C 131 172 131 172 131 172 L 131 190.56155300614688 C 131 190.56155300614688 131 190.56155300614688 131 190.56155300614688 C 131 190.56155300614688 131 190.56155300614688 131 190.56155300614688 C 131 190.56155300614688 131 190.56155300614688 131 190.56155300614688 C 131 190.56155300614688 131 190.56155300614688 131 190.56155300614688 Z" transform="translate(0,0)" visibility="visible"></path>
                        <g stroke="#7cb5ec" stroke-width="1" stroke-linejoin="round" x="131" y="172" r="69" innerR="0" start="-1.571" end="-1.39">
                          <path fill="#7cb5ec" d="M 130.98594654894504 123.20963311010813 C 135.37677870423596 123.20900074814081 139.08714355173737 123.44653711119415 143.4064167052544 124.00478761097318 L 131 172 C 131 172 131 172 131 172 Z" transform="translate(0,0)" visibility="visible"></path>
                        </g>
                        <g stroke="#434348" stroke-width="1" stroke-linejoin="round" x="131" y="172" r="69" innerR="0" start="-1.39" end="-0.284">
                          <path fill="#434348" d="M 143.40709545943696 124.00487533998677 C 169.80117349241982 127.41640774081705 189.71747239919512 140.11596875468243 197.2358274462527 158.32858525041695 L 131 172 C 131 172 131 172 131 172 Z" transform="translate(0,0)" visibility="visible"></path>
                        </g>
                        <g stroke="#90ed7d" stroke-width="1" stroke-linejoin="round" x="131" y="172" r="69" innerR="0" start="-0.284" end="4.712">
                          <path fill="#90ed7d" d="M 197.2360207859425 158.32905360912795 C 207.91369409967226 184.19582871498054 186.9147819659983 211.28568424912007 150.3336377964468 218.83593945655304 C 113.7524936268953 226.386194663986 75.4416525277873 211.53772149672466 64.76397921405751 185.67094639087205 C 54.08630590032774 159.80417128501946 75.08521803400174 132.7143157508799 111.66636220355323 125.16406054344695 C 118.27094206423308 123.80089204180365 124.0922869025267 123.21157703088345 130.97247035418678 123.209635981484 L 131 172 C 131 172 131 172 131 172 C 131 172 131 172 131 172 C 131 172 131 172 131 172 C 131 172 131 172 131 172 Z" transform="translate(0,0)" visibility="visible"></path>
                        </g>
                        <g stroke="#f7a35c" stroke-width="1" stroke-linejoin="round" x="131" y="172" r="69" innerR="0" start="4.712" end="4.712">
                          <path fill="#f7a35c" d="M 130.97316035413323 123.20963578925952 C 130.9729177533313 123.20963585598703 130.97271295498774 123.20963591304104 130.97247035418678 123.209635981484 L 131 172 C 131 172 131 172 131 172 Z" transform="translate(0,0)" visibility="hidden"></path>
                        </g>
                      </g>
                      <g class="highcharts-markers highcharts-series-0" transform="translate(10,41) scale(1 1)"></g>
                    </g>
                    <g class="highcharts-button" style="cursor:default;" stroke-linecap="round" transform="translate(260,10)">
                      <title>Chart context menu</title>
                      <rect x="0.5" y="0.5" width="24" height="22" fill="white" stroke="none" stroke-width="1" rx="2" ry="2"></rect>
                      <path fill="#E0E0E0" d="M 6 6.5 L 20 6.5 M 6 11.5 L 20 11.5 M 6 16.5 L 20 16.5" stroke="#666" stroke-width="3"></path>
                      <text x="0" style="color:black;fill:black;" y="12"></text>
                    </g>
                    <text x="147" text-anchor="middle" class="highcharts-title" style="color:#fffff  ;font-size:18px;font:bold 14px &quot;Trebuchet MS&quot;, Verdana, sans-serif;fill:#fffff  ;width:230px;" y="24">
                      <tspan>Step Wise Loan%</tspan>
                    </text>
                    <g class="highcharts-data-labels highcharts-series-0 highcharts-tracker" visibility="visible" transform="translate(10,41) scale(1 1)" opacity="1" style="cursor:pointer;">
                      <path fill="none" d="M 196.36401952255738 93.5322668412021 C 191.36401952255738 93.5322668412021 139.2225093780009 101.49954218219014 138.2285796729678 112.45454577604863 L 137.23464996793473 123.40954936990713" stroke="#000000" stroke-width="1"></path>
                      <path fill="none" d="M 224.1816307401944 127 C 219.1816307401944 127 191.94577795172646 119.43171523118028 184.57870589162766 127.60032889944195 L 177.21163383152887 135.76894256770362" stroke="#000000" stroke-width="1"></path>
                      <path fill="none" d="M 49.662590786973254 235.03649700485346 C 54.662590786973254 235.03649700485346 76.4095714546256 228.6358660063543 83.00841446560493 219.83499838341794 L 89.60725747658425 211.0341307604816" stroke="#000000" stroke-width="1"></path>
                      <path fill="none" d="M 66.05771371594273 93.20963056921157 C 71.05771371594273 93.20963056921157 131 101.20963056921157 131 112.20963056921157 L 131 123.20963056921157" stroke="#000000" stroke-width="1"></path>
                      <g style="cursor:pointer;" transform="translate(201,84)">
                        <text x="5" style="font-size:11px;font-weight:bold;color:#000000;text-shadow:0 0 6px #FFFFFF, 0 0 3px #FFFFFF;fill:#000000;text-rendering:geometricPrecision;" y="16">
                          <tspan style="font-weight:bold">Step 0</tspan>
                        </text>
                      </g>
                      <g style="cursor:pointer;" transform="translate(229,117)">
                        <text x="5" style="font-size:11px;font-weight:bold;color:#000000;text-shadow:0 0 6px #FFFFFF, 0 0 3px #FFFFFF;fill:#000000;text-rendering:geometricPrecision;" y="16">
                          <tspan style="font-weight:bold">Step 1</tspan>
                        </text>
                      </g>
                      <g style="cursor:pointer;" transform="translate(0,225)">
                        <text x="5" style="font-size:11px;font-weight:bold;color:#000000;text-shadow:0 0 6px #FFFFFF, 0 0 3px #FFFFFF;fill:#000000;text-rendering:geometricPrecision;" y="16">
                          <tspan style="font-weight:bold">Step 2</tspan>
                        </text>
                      </g>
                      <g style="cursor:pointer;" transform="translate(16,83)">
                        <text x="5" style="font-size:11px;font-weight:bold;color:#000000;text-shadow:0 0 6px #FFFFFF, 0 0 3px #FFFFFF;fill:#000000;text-rendering:geometricPrecision;" y="16">
                          <tspan style="font-weight:bold">Step 4</tspan>
                        </text>
                      </g>
                    </g>
                    <g class="highcharts-legend">
                      <g>
                        <g></g>
                      </g>
                    </g>
                    <g class="highcharts-tooltip" style="cursor:default;padding:0;pointer-events:none;white-space:nowrap;" transform="translate(1,-9999)" opacity="0" visibility="visible">
                      <path fill="none" d="M 3.5 0.5 L 226.5 0.5 C 229.5 0.5 229.5 0.5 229.5 3.5 L 229.5 31.5 C 229.5 34.5 229.5 34.5 226.5 34.5 L 3.5 34.5 C 0.5 34.5 0.5 34.5 0.5 31.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)"></path>
                      <path fill="none" d="M 3.5 0.5 L 226.5 0.5 C 229.5 0.5 229.5 0.5 229.5 3.5 L 229.5 31.5 C 229.5 34.5 229.5 34.5 226.5 34.5 L 3.5 34.5 C 0.5 34.5 0.5 34.5 0.5 31.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)"></path>
                      <path fill="none" d="M 3.5 0.5 L 226.5 0.5 C 229.5 0.5 229.5 0.5 229.5 3.5 L 229.5 31.5 C 229.5 34.5 229.5 34.5 226.5 34.5 L 3.5 34.5 C 0.5 34.5 0.5 34.5 0.5 31.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)"></path>
                      <path fill="rgba(249, 249, 249, .85)" d="M 3.5 0.5 L 226.5 0.5 C 229.5 0.5 229.5 0.5 229.5 3.5 L 229.5 31.5 C 229.5 34.5 229.5 34.5 226.5 34.5 L 3.5 34.5 C 0.5 34.5 0.5 34.5 0.5 31.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#90ed7d" stroke-width="1"></path>
                      <text x="8" style="font-size:12px;color:#333333;fill:#333333;" y="20">
                        <tspan style="font-weight:bold">Step 2</tspan>
                        <tspan dx="0">: 79.5209083356411 % Loans</tspan>
                      </text>
                    </g>
                    <text x="284" text-anchor="end" style="cursor:pointer;color:#909090;font-size:9px;fill:#909090;" y="395">Highcharts.com</text>
                  </svg>
                  <div class="highcharts-contextmenu" style="position: absolute; z-index: 1000; padding: 24px; display: none; right: -14px; top: 8px;">
                    <div style="box-shadow: rgb(136, 136, 136) 3px 3px 10px; border: 1px solid rgb(160, 160, 160); padding: 5px 0px; background: rgb(255, 255, 255);">
                      <div style="cursor: pointer; padding: 0px 10px; color: rgb(48, 48, 48); font-size: 11px; background: none;">Print chart</div>
                      <hr>
                      <div style="cursor: pointer; padding: 0px 10px; color: rgb(48, 48, 48); font-size: 11px; background: none;">Download PNG image</div>
                      <div style="cursor: pointer; padding: 0px 10px; color: rgb(48, 48, 48); font-size: 11px; background: none;">Download JPEG image</div>
                      <div style="cursor: pointer; padding: 0px 10px; color: rgb(48, 48, 48); font-size: 11px; background: none;">Download PDF document</div>
                      <div style="cursor: pointer; padding: 0px 10px; color: rgb(48, 48, 48); font-size: 11px; background: none;">Download SVG vector image</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        
        <div class="col-md-8">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Status Of <b>New Closed &amp; Open Loans </b> As per Workload Uploaded as on Date:20-Apr</h3>
            </div>
            <div class="box-body">
              <!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>HighCharts</title>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
              <script type="text/javascript">
		$(document).ready(function() {
			var options = {
	            chart: {
	                renderTo: 'container',
	                type: 'column',
	                marginRight: 100,
	                marginBottom: 25
	            },
		    credits: {
			  enabled: false
		},
		exporting: {
				 enabled: false
		},	
	            title: {
	                text: 'Status of Loans in Each Steps',
	                x: -20 //center
	            },
	            subtitle: {
	                text: '',
	                x: -10
	            },
	            xAxis: {
	                categories: []
	            },
	            yAxis: {
	                title: {
	                    text: 'Loan Count'
	                },
	                plotLines: [{
	                    value: 0,
	                    width: 1,
	                    color: '#808080'
	                }]
	            },
	            tooltip: {
	                formatter: function() {
	                        return '<b>'+ this.series.name +'</b>'+
	                        ':'+ this.y;
				   // this.x +': '+ this.y;

	                }
	            },
	            legend: {
	                layout: 'vertical',
	                align: 'right',
	                verticalAlign: 'top',
	                x: -10,
	                y: 100,
	                borderWidth: 0
	            },
	            series: []
	        }
	        
	        $.getJSON("div/data_pipeline.php", function(json) {
			options.xAxis.categories = json[0]['data'];
	        	options.series[0] = json[1];
	        	options.series[1] = json[2];
	        	options.series[2] = json[3];
		        chart = new Highcharts.Chart(options);
	        });
	    });
		</script>
              <!-- <script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>-->
              <div id="container" style="min-width: 400px; max-width:100%; height: 400px; margin: 0 auto" data-highcharts-chart="2">
                <div class="highcharts-container" id="highcharts-4" style="position: relative; overflow: hidden; width: 639px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                  <svg version="1.1" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="639" height="400">
                    <desc>Created with Highcharts 4.2.4</desc>
                    <defs>
                      <clippath id="highcharts-5">
                        <rect x="0" y="0" width="473" height="322"></rect>
                      </clippath>
                    </defs>
                    <rect x="0" y="0" width="639" height="400" fill="#FFFFFF" class=" highcharts-background"></rect>
                    <path fill="none" d="M 66 375.5 L 539 375.5" stroke="#808080" stroke-width="1" visibility="visible"></path>
                    <g class="highcharts-grid"></g>
                    <g class="highcharts-grid">
                      <path fill="none" d="M 66 375.5 L 539 375.5" stroke="#D8D8D8" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 66 329.5 L 539 329.5" stroke="#D8D8D8" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 66 283.5 L 539 283.5" stroke="#D8D8D8" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 66 237.5 L 539 237.5" stroke="#D8D8D8" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 66 191.5 L 539 191.5" stroke="#D8D8D8" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 66 145.5 L 539 145.5" stroke="#D8D8D8" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 66 99.5 L 539 99.5" stroke="#D8D8D8" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 66 52.5 L 539 52.5" stroke="#D8D8D8" stroke-width="1" opacity="1"></path>
                    </g>
                    <g class="highcharts-axis">
                      <path fill="none" d="M 160.5 375 L 160.5 385" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 254.5 375 L 254.5 385" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 349.5 375 L 349.5 385" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 443.5 375 L 443.5 385" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 539.5 375 L 539.5 385" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 65.5 375 L 65.5 385" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                      <path fill="none" d="M 66 375.5 L 539 375.5" stroke="#C0D0E0" stroke-width="1"></path>
                    </g>
                    <g class="highcharts-axis">
                      <text x="27.625" text-anchor="middle" transform="translate(0,0) rotate(270 27.625 214)" class=" highcharts-yaxis-title" style="color:#707070;fill:#707070;" y="214">
                        <tspan>Loan Count</tspan>
                      </text>
                    </g>
                    <g class="highcharts-series-group">
                      <g class="highcharts-series highcharts-series-0 highcharts-tracker" transform="translate(66,53) scale(1 1)" style="" clip-path="url(#highcharts-5)">
                        <rect x="20.5" y="312.5" width="16" height="10" stroke="#FFFFFF" stroke-width="1" fill="#7cb5ec" rx="0" ry="0"></rect>
                        <rect x="114.5" y="264.5" width="16" height="58" stroke="#FFFFFF" stroke-width="1" fill="#7cb5ec" rx="0" ry="0"></rect>
                        <rect x="209.5" y="58.5" width="16" height="264" stroke="#FFFFFF" stroke-width="1" fill="#7cb5ec" rx="0" ry="0"></rect>
                        <rect x="304.5" y="293.5" width="16" height="29" stroke="#FFFFFF" stroke-width="1" fill="#7cb5ec" rx="0" ry="0"></rect>
                        <rect x="398.5" y="322.5" width="16" height="0" stroke="#FFFFFF" stroke-width="1" fill="#7cb5ec" rx="0" ry="0"></rect>
                      </g>
                      <g class="highcharts-markers highcharts-series-0" transform="translate(66,53) scale(1 1)" clip-path="none"></g>
                      <g class="highcharts-series highcharts-series-1 highcharts-tracker" transform="translate(66,53) scale(1 1)" style="" clip-path="url(#highcharts-5)">
                        <rect x="39.5" y="317.5" width="16" height="5" stroke="#FFFFFF" stroke-width="1" fill="#434348" rx="0" ry="0"></rect>
                        <rect x="133.5" y="269.5" width="16" height="53" stroke="#FFFFFF" stroke-width="1" fill="#434348" rx="0" ry="0"></rect>
                        <rect x="228.5" y="63.5" width="16" height="259" stroke="#FFFFFF" stroke-width="1" fill="#434348" rx="0" ry="0"></rect>
                        <rect x="323.5" y="293.5" width="16" height="29" stroke="#FFFFFF" stroke-width="1" fill="#434348" rx="0" ry="0"></rect>
                        <rect x="417.5" y="322.5" width="16" height="0" stroke="#FFFFFF" stroke-width="1" fill="#434348" rx="0" ry="0"></rect>
                      </g>
                      <g class="highcharts-markers highcharts-series-1" transform="translate(66,53) scale(1 1)" clip-path="none"></g>
                      <g class="highcharts-series highcharts-series-2 highcharts-tracker" transform="translate(66,53) scale(1 1)" style="" clip-path="url(#highcharts-5)">
                        <rect x="58.5" y="317.5" width="16" height="5" stroke="#FFFFFF" stroke-width="1" fill="#90ed7d" rx="0" ry="0"></rect>
                        <rect x="152.5" y="317.5" width="16" height="5" stroke="#FFFFFF" stroke-width="1" fill="#90ed7d" rx="0" ry="0"></rect>
                        <rect x="247.5" y="317.5" width="16" height="5" stroke="#FFFFFF" stroke-width="1" fill="#90ed7d" rx="0" ry="0"></rect>
                        <rect x="341.5" y="322.5" width="16" height="0" stroke="#FFFFFF" stroke-width="1" fill="#90ed7d" rx="0" ry="0"></rect>
                        <rect x="436.5" y="322.5" width="16" height="0" stroke="#FFFFFF" stroke-width="1" fill="#90ed7d" rx="0" ry="0"></rect>
                      </g>
                      <g class="highcharts-markers highcharts-series-2" transform="translate(66,53) scale(1 1)" clip-path="none"></g>
                    </g>
                    <text x="300" text-anchor="middle" class="highcharts-title" style="color:#333333;font-size:18px;fill:#333333;width:575px;" y="24">
                      <tspan>Status of Loans in Each Steps</tspan>
                    </text>
                    <g class="highcharts-legend" transform="translate(474,110)">
                      <g>
                        <g>
                          <g class="highcharts-legend-item" transform="translate(8,3)">
                            <text x="21" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" y="15">active_loan_count</text>
                            <rect x="0" y="4" width="16" height="12" fill="#7cb5ec"></rect>
                          </g>
                          <g class="highcharts-legend-item" transform="translate(8,21)">
                            <text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start">processedloan</text>
                            <rect x="0" y="4" width="16" height="12" fill="#434348"></rect>
                          </g>
                          <g class="highcharts-legend-item" transform="translate(8,39)">
                            <text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start">pendingloan</text>
                            <rect x="0" y="4" width="16" height="12" fill="#90ed7d"></rect>
                          </g>
                        </g>
                      </g>
                    </g>
                    <g class="highcharts-axis-labels highcharts-xaxis-labels">
                      <text x="113.3" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:85px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="394" opacity="1">
                        <tspan>Step 0</tspan>
                      </text>
                      <text x="207.89999999999998" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:85px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="394" opacity="1">
                        <tspan>Step 1</tspan>
                      </text>
                      <text x="302.5" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:85px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="394" opacity="1">
                        <tspan>Step 2</tspan>
                      </text>
                      <text x="397.09999999999997" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:85px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="394" opacity="1">
                        <tspan>Step 3</tspan>
                      </text>
                      <text x="491.7" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:85px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="394" opacity="1">
                        <tspan>Step 4</tspan>
                      </text>
                    </g>
                    <g class="highcharts-axis-labels highcharts-yaxis-labels">
                      <text x="51" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:201px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="378" opacity="1">0k</text>
                      <text x="51" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:201px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="332" opacity="1">1k</text>
                      <text x="51" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:201px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="286" opacity="1">2k</text>
                      <text x="51" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:201px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="240" opacity="1">3k</text>
                      <text x="51" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:201px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="194" opacity="1">4k</text>
                      <text x="51" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:201px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="148" opacity="1">5k</text>
                      <text x="51" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:201px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="102" opacity="1">6k</text>
                      <text x="51" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:201px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="56" opacity="1">7k</text>
                    </g>
                    <g class="highcharts-tooltip" style="cursor:default;padding:0;pointer-events:none;white-space:nowrap;" transform="translate(0,-9999)">
                      <path fill="none" d="M 3.5 0.5 L 13.5 0.5 C 16.5 0.5 16.5 0.5 16.5 3.5 L 16.5 13.5 C 16.5 16.5 16.5 16.5 13.5 16.5 L 3.5 16.5 C 0.5 16.5 0.5 16.5 0.5 13.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)"></path>
                      <path fill="none" d="M 3.5 0.5 L 13.5 0.5 C 16.5 0.5 16.5 0.5 16.5 3.5 L 16.5 13.5 C 16.5 16.5 16.5 16.5 13.5 16.5 L 3.5 16.5 C 0.5 16.5 0.5 16.5 0.5 13.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)"></path>
                      <path fill="none" d="M 3.5 0.5 L 13.5 0.5 C 16.5 0.5 16.5 0.5 16.5 3.5 L 16.5 13.5 C 16.5 16.5 16.5 16.5 13.5 16.5 L 3.5 16.5 C 0.5 16.5 0.5 16.5 0.5 13.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)"></path>
                      <path fill="rgba(249, 249, 249, .85)" d="M 3.5 0.5 L 13.5 0.5 C 16.5 0.5 16.5 0.5 16.5 3.5 L 16.5 13.5 C 16.5 16.5 16.5 16.5 13.5 16.5 L 3.5 16.5 C 0.5 16.5 0.5 16.5 0.5 13.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5"></path>
                      <text x="8" style="font-size:12px;color:#333333;fill:#333333;" y="20"></text>
                    </g>
                  </svg>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </section>
  </div>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.2.0.min.js"></script>
  <footer class="main-footer">
    <div class="pull-right hidden-xs"></div>
    <strong>Copyright © <a href="http://quatrro.com/">Quatrro Mortgage Solutions</a>.</strong> All rights reserved. </footer>
  <div class="control-sidebar-bg" style="position: fixed; height: auto;"></div>
  <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
  <!-- For 3d graph-->
  <script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/highcharts-3d.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/exporting.js"></script>
  <!-- End of 3d graph-->
  <!-- For column graph in jqwidgets -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jqxcore.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jqxchart.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jqxdata.js"></script>
  <!-- End of column graph -->
  <!-- Password strength -->
  <script src="<?php echo base_url(); ?>assets/js/bootstrap-strength.js"></script>
  <script>		
		$('.newpwd').bootstrapStrength({
			slimBar:true,
			minLenght: 8,
			upperCase: 1,
			numbers: 1,
			specialchars: 1
		});
		$('.cnewpwd').bootstrapStrength({
			slimBar:true,
			minLenght: 8,
			upperCase: 1,
			numbers: 1,
			specialchars: 1
		});		
		</script>

  <!-- Password strength -->
</div>

<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display:none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        <h4 class="modal-title" id="myModalLabel">Notification</h4>
      </div>
      <div class="modal-body" id="text123">
      <div class="panel-group custom-accord" id="accordion" role="tablist" aria-multiselectable="true">
       <?php 
        $i=0;
        $j=0;
       foreach($notify['data'] as $res)  { //print_r($res);
          $i++;
          $j++;
        ?>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">              
                <div class="row">
                  <div class="col-sm-3">
                    <div class="show-head">
                      <label>Request From</label>
                      <p><?php echo $res['senderName'];?></p>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="show-head">
                      <label>Response From</label>
                      <p><?php echo $res['receiverName'];?></p>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="show-head">
                      <label>Days Pending</label>
                      <p><?php 
                          $now = date_create(date('Y-m-d H:i:s'));
                          
                          $then = date_create($res['creationDate']); //print_r($now); die();
                          $diff=date_diff($then,$now); //print_r($diff); die();
                          
                          echo $diff->format("%a days");
                          
                          
                          ?></p>
                    </div>
                  </div>
                </div>
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $j;?>" aria-expanded="true" aria-controls="collapse<?php echo $j;?>"></a>
            </h4>
          </div>
          <div id="collapse<?php echo $j;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $j;?>">
            <div class="panel-body">
            <div class="row">
              <div class="col-sm-4">
                <p><b>Loan Number: </b><?php echo $res['loanNumber']; ?></p>
                <p><b>Date: </b><?php  echo date('m-d-Y',strtotime($res['creationDate']));?></p>
                <p><b>Time: </b><?php echo date("H:i",strtotime($res['creationDate']));?></p>
              </div>
              <div class="col-sm-7 pl">
                <div class="comment-wrap">                          
                          <?php 
                            $i=0;
                            foreach($res['comments'] as $comm):
                            $i++;
                            if($i%2==0){ ?>
                               <div class="comments">
                              <?php 
                              
                                echo '<p><b>'.$comm['senderName'].':'.'</b>';
                                echo ''.$comm['comments'].'</p>'; 
                               
                              
                              ?>
                               <span class="time"><?php echo $comm['creationDate'];?></span>
                          </div> 
                          <?php   }else { ?>
                             <div class="comments">
                          <?php 
                          
                            echo '<p><b>'.$comm['senderName'].':'.'</b>';
                            echo ''.$comm['comments'].'</p>';  
                          
                          ?>
                          <span class="time"><?php echo $comm['creationDate'];?></span>
                          </div> 
                          <?php   }
                            ?>
                         
                          <?php endforeach;?>
                          </div>
              </div>
              <div class="col-sm-1 pl">
                <a href="<?php echo base_url('index.php/Notify/edit_records'). '/' . $res['recordId'];?>">
                            <button type="button" class="btn btn-info" id="button1">Open</button>
                          </a>
              </div>
            </div>
              
            </div>
          </div>
        </div>
        <?php }// endforeach; }?>
  </div>
        
      </div>
      <div class="modal-footer form-inline">
       <a href="<?php echo base_url('index.php/Notify/addRecord').'/'.'notify';?>">
		<button type="button" class="btn btn-primary"  style="float:left">
       Add New
        </button> </a> 
        <a href="<?php echo base_url('index.php/Notify/addRecord').'/'.'notify'; ?>">
		<button type="button" class="btn btn-search"  style="float:left">
      Search
        </button> </a>
        <select class="form-control search" style="float:left" onchange=changeProfileInformation2(this.value)>
        	<option value = "0">All</option>
        	<option value = "1">Today</option>
        	<option value = "2">This Week</option>
        	<option value = "3">Next Week</option>
        </select>
        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
        <input type="hidden" class="category" id="profile12" name="profile12"
							value="<?php echo base_url('/index.php/Notify/filterNotify');?>"></input>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div id="myModal2" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display:none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true">×</span> </button>
        <h4 class="modal-title" id="myModalLabel"><b>Follow Up</b></h4>
      </div>
      <div class="modal-body">
      
          <div class="modal-body" id="txtHintssss">
      <div class="panel-group custom-accord" id="accordion" role="tablist" aria-multiselectable="true">
       <?php 
        $i=0;
        $j=0;
        $k=0;
      //print_r($follow);
          foreach($follow as $row){
       //  print_r($row['comments'][0]['flag']);
          $i++;
          $j++;
        ?>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingOne" <?php  if((strtotime(date('Y-m-d H:i:s')) > strtotime($row['followupDate'])) && $row['comments'][0]['flag'] == 0){ echo "style='background:rgba(255, 0, 0, 0.38);'"; } ?>>
            <h4 class="panel-title">              
                <div class="row">
                  <div class="col-sm-3">
                    <div class="show-head">
                      <label>Loan Number</label>
                      <p><?php echo $row['loanNumber']; ?></p>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="show-head">
                      <label>Date</label>
                      <p><?php  echo date('m-d-Y',strtotime($row['followupDate']));?></p>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="show-head">
                      <label>Time</label>
                      <p><?php echo date("H:i",strtotime($row['followupDate']));?></p>
                    </div>
                  </div>
                </div>
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1<?php echo $j;?>" aria-expanded="true" aria-controls="collapse<?php echo $j;?>"></a>
            </h4>
          </div>
          <div id="collapse1<?php echo $j;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $j;?>">
            <div class="panel-body">
            <div class="row">
              <div class="col-sm-2">
                <label class="comm">Comments</label>
              </div>
              <div class="col-sm-7 pl">
                <div class="comment-wrap">                          
                          <?php 
                            $i=0;
                            foreach($row['comments'] as $comm):
                            
                           //print_r($comm);
                            $i++;
                            if($i%2==0){ ?>
                               <div class="comments">
                              <?php 
                              
                                echo '<p><b>'.$comm['senderName'] .':'.'</b>';
                                echo ''.$comm['comments'] .'</p>'; 
                              
                              ?>
							  <span class="time"><?php echo $comm['creationDate'];?></span>
                          </div> 
                          <?php   }else { ?>
                             <div class="comments">
                          <?php 
                          
                            echo '<p><b>'.$comm['senderName'] .':'.'</b>';
                            echo ''.$comm['comments'] .'</p>';  
                          
                          ?>
						  <span class="time"><?php echo $comm['creationDate'];?></span>
                          </div> 
                          <?php   }
                            ?>
                         
                          <?php endforeach;?>
                          </div>
              </div>
			  <div class="col-sm-3">
              <form name="form_<?php echo $k; ?>" action="<?php echo base_url('index.php/Notify/edit_records'). '/' . $row['id'];?>" method="post">
              <div class="pl floatL mR5">
                <a href="<?php echo base_url('index.php/Notify/edit_records'). '/' . $row['id'];?>">
                <input type="hidden" name="open_text" value="<?php echo $row['id'].'_1'; ?>" />
                            <input type="submit" class="btn btn-info" id="button1" value="open" name="open">
                          </a>
              </div>
              
              <div class="pl floatL">
												<a
													href="<?php echo base_url('index.php/Notify/edit_records'). '/' . $row['id'];?>">
													<input type="hidden" name="snooze_text" value="<?php echo $row['id'].'_2'; ?>" />
													<input type="submit" class="btn btn-info" id="button1" value="snooze" name="snooze">
												</a>
											</div>
											</form>
				</div>
            </div>
              
            </div>
          </div>
        </div>
        <?php 
          $k++;
          }// endforeach; }?>
  </div>
      </div>
      <div class="modal-footer form-inline">
         <a href="<?php echo base_url('index.php/Notify/addRecord').'/'.'follow';?>">
		<button type="button" class="btn btn-primary"  style="float:left">
       Add New
        </button> </a> 
         <a href="<?php echo base_url('index.php/Notify/addRecord').'/'.'follow';?>">
		<button type="button" class="btn btn-search"  style="float:left">
      Search
        </button> </a>
        <select class="form-control search" style="float:left"  onchange=changeProfileInformation(this.value)>
        	<option value = "0">All</option>
        	<option value = "1">Today</option>
        	<option value = "2">This Week</option>
        	<option value = "3">Next Week</option>
        </select>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="hidden" class="select category" id="id" name="id" value="<?php echo $records->id;?>"></input>
         <input type="hidden" class="category" id="profile" name="profile"
							value="<?php echo base_url('/index.php/Notify/filterfollowup');?>"></input>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</div>
<!-- This div is for div class wrapper included in header.php -->
<div id="window-resizer-tooltip" style="display: none;"><a href="#/home#" title="Edit settings"></a><span class="tooltipTitle">Window size: </span><span class="tooltipWidth" id="winWidth">1280</span> x <span class="tooltipHeight" id="winHeight">984</span><br>
  <span class="tooltipTitle">Viewport size: </span><span class="tooltipWidth" id="vpWidth">1280</span> x <span class="tooltipHeight" id="vpHeight">879</span></div>


</body>
</html>

<script>
changeProfileInformation = function (str) {

    var reqesturl = $('#profile').val();
    var value = str;
  //alert(reqesturl);
    $.ajax({
        type: 'POST',
        url: reqesturl,
        data: {
            id:value
        },
        success: function (res) {
            $("#txtHintssss").html(res);
        },
        dataType: 'html'
    });
}

changeProfileInformation2 = function (str) {

    var reqesturl = $('#profile12').val();
    var value = str;
  //alert(reqesturl);
    $.ajax({
        type: 'POST',
        url: reqesturl,
        data: {
            id:value
        },
        success: function (res) {
            $("#text123").html(res);
        },
        dataType: 'html'
    });
}
</script>
