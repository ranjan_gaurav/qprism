   <?php error_reporting(0);?>
      <div class="panel-group custom-accord" id="accordion" role="tablist" aria-multiselectable="true">
       <?php 
        $i=0;
        $j=0;
       foreach($filter as $res)  { //print_r($res);
          $i++;
          $j++;
        ?>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">              
                <div class="row">
                  <div class="col-sm-3">
                    <div class="show-head">
                      <label>Request From</label>
                      <p><?php echo $res['senderName'];?></p>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="show-head">
                      <label>Response From</label>
                      <p><?php echo $res['receiverName'];?></p>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="show-head">
                      <label>Days Pending</label>
                      <p><?php 
                          $now = new DateTime();
                          
                          $then = $res['creationDate'];
                          $then = new DateTime($then);
                          
                          $sinceThen = $then->diff($now);
                          
                          echo $sinceThen->h.' days';
                          
                          
                          ?></p>
                    </div>
                  </div>
                </div>
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $j;?>" aria-expanded="true" aria-controls="collapse<?php echo $j;?>"></a>
            </h4>
          </div>
          <div id="collapse<?php echo $j;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $j;?>">
            <div class="panel-body">
            <div class="row">
              <div class="col-sm-4">
                <p><b>Loan Number: </b><?php echo $res['loanNumber']; ?></p>
                <p><b>Date: </b><?php  echo date('m-d-Y',strtotime($res['creationDate']));?></p>
                <p><b>Time: </b><?php echo date("H:i",strtotime($res['creationDate']));?></p>
              </div>
              <div class="col-sm-7 pl">
                <div class="comment-wrap">                          
                          <?php 
                            $i=0;
                            foreach($res['comments'] as $comm):
                            $i++;
                            if($i%2==0){ ?>
                               <div class="comments">
                              <?php 
                              
                                echo '<p><b>'.$comm['senderName'].':'.'</b>';
                                echo ''.$comm['comments'].'</p>'; 
                              
                              ?>
                          </div> 
                          <?php   }else { ?>
                             <div class="comments">
                          <?php 
                          
                            echo '<p><b>'.$comm['senderName'].':'.'</b>';
                            echo ''.$comm['comments'].'</p>';  
                          
                          ?>
                          </div> 
                          <?php   }
                            ?>
                         
                          <?php endforeach;?>
                          </div>
              </div>
              <div class="col-sm-1 pl">
                <a href="<?php echo base_url('index.php/Notify/edit_records'). '/' . $res['recordId'];?>">
                            <button type="button" class="btn btn-info" id="button1">Open</button>
                          </a>
              </div>
            </div>
              
            </div>
          </div>
        </div>
        <?php }// endforeach; }?>
  </div>