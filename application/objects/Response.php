<?php ob_start();
	 /**
	 * @author : Anjani Kr. Gupta
	 * Response class
	 */
 class Response
 {
 	private $status = false;
 	private $msg = "";
 	private $responseId = "";
 	private $error = "";
 	private $objArray = "";

 	/**
 	 * @method: setStatus
 	 * Description: set responce status
 	 * @param: boolean status
 	 */
 	public function setStatus($_status)
 	{
 		$this->status = $_status;
 	}
 	
 	/**
 	 * @method: getStatus
 	 * Description: Get status
 	 * @return: return status;
 	 */
 	public function getStatus()
 	{
 		return $this->status;
 	}
 	
 	/**
 	 * @method: setResponseId
 	 * Description: set responce id
 	 * @param: boolean id
 	 */
 	public function setResponseId($_id)
 	{
 		$this->responseId = $_id;
 	}
 	
 	/**
 	 * @method: getResponseId
 	 * Description: getResponseId
 	 * @return: responseId;
 	 */
 	public function getResponseId()
 	{
 		return $this->responseId;
 	}
 	
 	/**
 	 * @method: setMsg
 	 * Description: set responce message
 	 * @param: deal id
 	 */
 	public function setMsg($_msg)
 	{
 		$this->msg = $_msg;
 	}
 	
 	/**
 	 * @method: getMsg
 	 * Description: return responce message
 	 * @return: return String;
 	 */
 	public function getMsg()
 	{
 		return $this->msg;
 	}
 	
 	/**
 	 * @method: setError
 	 * Description: set error message
 	 * @param: deal id
 	 */
 	public function setError($_error)
 	{
 		$this->error = $_error;
 	}
 	
 	/**
 	 * @method: getError
 	 * Description: return error message
 	 * @return: return String;
 	 */
 	public function getError()
 	{
 		return $this->error;
 	}
 	
  	/**
 	 * @method: setObjArray
 	 * Description: set object array
 	 * @param: object array
 	 */
 	public function setObjArray($_objArray)
 	{
 		$this->objArray = $_objArray;
 	}
 	
 	/**
 	 * @method: getObjArray
 	 * Description: return object array
 	 * @return: return String;
 	 */
 	public function getObjArray()
 	{
 		return $this->objArray;
 	}
 	
 	public function toArray()
 	{
 		return (array) $this;
 	}
 	
 }
 ?>
 	