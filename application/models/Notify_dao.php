<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 29th July 2016
 * Description : This Dao class is responsible of all the database related operation for API
 */

 class Notify_dao extends CI_Model
 {
 	
 	public function __construct()
 	{
        parent::__construct();
        include_once './application/objects/Response.php';
        //date_default_timezone_set("Asia/Kolkata");
 	}
 	
 	/**
 	* @author:  Anjani Kr. Gupta
 	* @method: saveRecords
 	* @Desc : save notification data and send notification
 	* @return: status
 	* Date: 20th Sept 2016
 	*/
 	public function saveRecords($data)
	{
		$response = new Response();
		try
 		{
 			$this->db->trans_start();
 			if($data['action']==NO_ACTION){
 				$comments = $data['comments'];
 			}else{
 				$comments = "";
 			}
 			$record = array(
 					'loanNumber' => $data['loanNumber'],
 					'borrowerName' => $data['borrowerName'],
 					'borrowerEmail' => $data['borrowerEmail'],
 					'borrowerPhone' => $data['borrowerPhone'],
 					'currentStage' => $data['currentStage'],
 					'loanAmount' => $data['loanAmount'],
 					'loanOfficer' => $data['loanOfficer'],
 					'loanProcessor' => $data['loanProcessor'],
 					'processingManager' => $data['processingManager'],
 					'CallType' => $data['callType'],
 					'CallCategory' => $data['callCategory'],
 					'CallCategoryNotes' => $data['callCategoryNotes'],
 					'action' => $data['action'],
 					'comments' => $comments,
 					'addedById' => $data['addedById'],
 					'addedByName' => $data['addedByName']
 			);
 			
		 		$result = $this->db->insert('records', $record);
            	$recordId = $this->db->insert_id();
            	if($data['action']==FOLLOW_UP){
            		$followUpData = array(
		 					'followupDate' => date('Y-m-d H:i:s',strtotime($data['followupDateTime'].$data['followupTime'])),
		 					'comments' => $data['comments'],
		 					'record_id' => $recordId
 					);
                 	$this->db->set('creationDate',date('Y-m-d H:i:s'));
            		$this->db->insert('follow_up', $followUpData);  //echo $this->db->last_query(); die();
            		$this->db->trans_complete();
            	}elseif ($data['action']==NOTIFY){
            		$notificationData = array(
            				'record_id' => $recordId,
            				'comments' => $data['comments'],
            				'senderId' => SENDER_ID,
            				'senderName' => SENDER_NAME,
            				'receiverId' => RECEIVER_ID,
            				'receiverName' => RECEIVER_NAME
            		);
            		$this->db->insert('notification', $notificationData);
            		$this->db->trans_complete();
            	}else{
            		$this->db->trans_complete();
            	}
            	if ($this->db->trans_status() === FALSE){
            		$response->setStatus(false);
            		$response->setMsg("Error in saving data.");
            		$response->setObjArray(NULL);
            	}else{
            		$response->setStatus(true);
            		$response->setMsg("Data saved successfully.");
            		$response->setObjArray($recordId);
            	}
 		}catch (Exception $e){
 			$response->setStatus(false);
	 		$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
 			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
 		}
 		return $response;
	}
	
	/**
	 * @author:  Anjani Kr. Gupta
	 * @method: searchLoan
	 * @Desc : search loan
	 * @return: status
	 * Date: 20th Sept 2016
	 */
	public function searchLoan($data)
	{
		$response = new Response();
		try
		{
			if($data['loanNo']!=NULL && $data['borrowerName']!=NULL){
				$where = " loanNo=".$this->db->escape($data['loanNo'])." AND last_name=".$this->db->escape($data['borrowerName']);
			}else if($data['loanNo']!=NULL && $data['borrowerName']==NULL){
				$where = " loanNo=".$this->db->escape($data['loanNo']);
			}else if($data['loanNo']==NULL && $data['borrowerName']!=NULL){
				$where = " last_name=".$this->db->escape($data['borrowerName']);
			}
			
			$query = "SELECT * FROM assignedloan WHERE ".$where;
			$result = $this->db->query($query);
			if($result->num_rows() > 0)
			{
				$response->setStatus(true);
				$response->setMsg("Data found");
				$response->setObjArray($result->result_array());
				//print_r($result->result_array());
			}else{
				$response->setStatus(false);
				$response->setMsg("Data not found");
				$response->setObjArray(NULL);
			}
		}catch (Exception $e){
			$response->setStatus(false);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
 public function getNotification($userId){
		$response = new Response();
		$notData = array();
		try
		{
			$query = "SELECT DISTINCT(record_id) FROM notification  WHERE receiverId=".$this->db->escape($userId);
			//print_r($query); die();
			$result = $this->db->query($query);
			if($result->num_rows() > 0)
			{
				foreach ($result->result() as $row){
					$n = $this->getNotificationData($userId, $row->record_id);
					array_push($notData, $n);
				}
				$response->setStatus(true);
				$response->setMsg("Data found");
				$response->setObjArray($notData);
			}else{
				$response->setStatus(false);
				$response->setMsg("Data not found");
				$response->setObjArray(NULL);
			}
		}catch (Exception $e){
			$response->setStatus(false);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
	public function getNotificationData($userId,$recordId){
		$n = array();
		$query = "SELECT N.*,R.loanNumber FROM notification N INNER JOIN records R on R.id = N.record_id WHERE N.receiverId=".$this->db->escape($userId)." ORDER BY N.creationDate DESC LIMIT 1";
		$result = $this->db->query($query);
		
		
		
		if($result->num_rows() > 0)
		{
			$n['recordId'] = $recordId;
			$n['loanNumber'] =$result->row()->loanNumber;
			$n['senderId'] = $result->row()->senderId;
			$n['senderName'] = $result->row()->senderName;
			$n['receiverId'] = $result->row()->receiverId;
			$n['receiverName'] =  $result->row()->receiverName;
			$n['creationDate'] =  $result->row()->creationDate;
			
			$query = "SELECT * FROM notification notf WHERE notf.record_id=".$this->db->escape($recordId)." ORDER BY notf.creationDate DESC";
			$result = $this->db->query($query);
			//print_r($query);die();
			$n['comments'] =  $result->result_array();
		}
		return $n;
	}

	
	public function getNotificationRecord($recordId){
		$response = new Response();
		$notData = array();
		try
		{
			$query = "SELECT * FROM records WHERE id=".$this->db->escape($recordId);
			$result = $this->db->query($query);
			if($result->num_rows() > 0)
			{
				foreach ($result->result() as $row){
					$notData['loanNumber'] = $row->loanNumber;
					$notData['borrowerName'] = $row->borrowerName;
					$notData['borrowerEmail'] = $row->borrowerEmail;
					$notData['borrowerPhone'] = $row->borrowerPhone;
					$notData['currentStage'] = $row->currentStage;
					$notData['loanAmount'] = $row->loanAmount;
					$notData['loanOfficer'] = $row->loanOfficer;
					$notData['loanProcessor'] = $row->loanProcessor;
					$notData['processingManager'] = $row->processingManager;
					$notData['CallType'] = $row->CallType;
					$notData['CallCategory'] = $row->CallCategory;
					$notData['CallCategoryNotes'] = $row->CallCategoryNotes;
					$notData['action'] = $row->action;
					$notData['comments'] = $row->comments;
					$notData['addedById'] = $row->addedById;
					$notData['addedByName'] = $row->addedByName;
					$notData['notificationData'] = $this->getNotificationData($row->addedById, $recordId);
				}
				$response->setStatus(true);
				$response->setMsg("Data found");
				$response->setObjArray($notData);
			}else{
				$response->setStatus(false);
				$response->setMsg("Data not found");
				$response->setObjArray(NULL);
			}
		}catch (Exception $e){
			$response->setStatus(false);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
	/*
	 * Notification alert
	 */
	public function notificationAlert($userId){
		$response = new Response();
		try
		{
			$query = "SELECT COUNT(*) count FROM notification WHERE receiverId=".$this->db->escape($userId). " AND flag=0";
			$result = $this->db->query($query);
			if($result->num_rows() > 0)
			{
				$response->setStatus(true);
				$response->setMsg("Got new notification");
				$response->setObjArray($result->row());
			}else{
				$response->setStatus(false);
				$response->setMsg("Non new notification");
				$response->setObjArray(NULL);
			}
		}catch (Exception $e){
			$response->setStatus(false);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
	public function getAllLoanNo($loanNo)
	{
			$r = array();
			$query = "SELECT loanNo FROM  assignedloan WHERE loanNo LIKE '".$loanNo."%'";
			$result = $this->db->query($query);
			if($result->num_rows() > 0)
			{
				foreach($result->result() as $row){
					array_push($r, $row->loanNo);
				}
				$response = $r;
				//print_r($r);die;
			}else{
				$response = $r;
			}
			
			return $response;
	}
	
	public function getAllBorrower($name)
	{
			$r = array();
			$query = "SELECT last_name,loanNo FROM  assignedloan WHERE last_name LIKE '".$name."%'";
			$result = $this->db->query($query);
			if($result->num_rows() > 0)
			{
				foreach($result->result() as $row){
					array_push($r, $row->last_name .', ' .'['. $row->loanNo .']');
				}
				$response = $r;
				//print_r($r);die;
			}else{
				$response = $r;
			}
			
			return $response;
	}
	
	public function followUpData($userId){
		$followup = array();
		$f = array();
		$query = "SELECT distinct records.* FROM records INNER JOIN follow_up on follow_up.record_id = records.id WHERE addedById=".$this->db->escape($userId)." AND action =1 ORDER BY follow_up.followupDate DESC";
		//print_r($query); die();
		$result = $this->db->query($query);
		if($result->num_rows()>0){
			foreach ($result->result() as $row){
				
			//	print_r($row); 
				
				$f['loanNumber'] = $row->loanNumber;
				$f['borrowerName'] = $row->borrowerName;
				$f['borrowerEmail'] = $row->borrowerEmail;
				$f['borrowerPhone'] = $row->borrowerPhone;
				$f['loanOfficer'] = $row->loanOfficer;
				$f['loanAmount'] = $row->loanAmount;
				$f['loanProcessor'] = $row->loanProcessor;
				$f['id'] = $row->id;
				$f['comments']  = $this->getRecordComments($row->id); //print_r($f['comments'][0]['followupDate']); die();
				$f['followupDate'] = (sizeof($f['comments']) > 0 )?$f['comments'][0]['followupDate']:"";
 				array_push($followup,$f);
			}
			
			//print_r($followup);die();
		}
		return $followup;
	}
	
	public function getRecordComments($recordId){
		$query = "SELECT * FROM follow_up WHERE record_id=".$this->db->escape($recordId)." ORDER BY followupDate desc";
		$result = $this->db->query($query);
		return $result->result_array();
	}
	

	
 }
?>