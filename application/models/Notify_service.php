<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 29th July 2016
 * Description : This Dao class is responsible of all the database related operation for API
 */

 class Notify_service extends CI_Model
 {
 	
 	
 	public function __construct()
 	{
        parent::__construct();
        $this->load->model('Notify_dao');
        include_once './application/objects/Response.php';
        //date_default_timezone_set("Asia/Kolkata");
 	}
 	
 	/**
 	* @author:  Anjani Kr. Gupta
 	* @method: saveRecords
 	* @Desc : save notification data and send notification
 	* @return: status
 	* Date: 20th Sept 2016
 	*/
 	public function saveRecords($data)
	{
 		
		$response = new Response();
		try
 		{	
 			$notifyDao = new Notify_dao();
 			$response = $notifyDao->saveRecords($data);
 			
 			/* if($res->getStatus()){
 				if($data['action']==NOTIFY){
 					//send mail
 					$response->setStatus($res->getStatus());
 					$response->setMsg($res->getMsg());
 					$response->setObjArray($res->getObjArray());
 				}
 			}else{
 				$response->setStatus($res->getStatus());
 				$response->setMsg($res->getMsg());
 				$response->setObjArray($res->getObjArray());
 			} */
 		}catch (Exception $e){
 			$response->setStatus(false);
 			$response->setMsg($e->getMessage());
 			$response->setError($e->getMessage());
 			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
 		}
 		return $response;
	}
	/**
	 * @author:  Anjani Kr. Gupta
	 * @method: searchLoan
	 * @Desc : search loan
	 * @return: status
	 * Date: 20th Sept 2016
	 */
	public function searchLoan($data)
	{
			
		$response = new Response();
		try
		{
			$notifyDao = new Notify_dao();
			$response = $notifyDao->searchLoan($data);
		}catch (Exception $e){
			$response->setStatus(false);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
 public function getNotification($userId){
		$response = new Response();
		try
		{
			$notifyDao = new Notify_dao();
			$response = $notifyDao->getNotification($userId); //print_r($response); die();
		}catch (Exception $e){
			$response->setStatus(false);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
	
	
	public function getfollowupList($userId)
	{
		/* $this->db->where('records.addedById',SENDER_ID);
		$this->db->select('follow_up.*,records.loanNumber,records.action');
		$this->db->from('records');
		$this->db->join('follow_up',"records.id = follow_up.record_id and records.action = '1'",'inner');
		$query = $this->db->get(); */
		
	//echo $this->db->last_query(); die();
		
		//return  $query->result_array();
		
		//print_r($result); die();
		$notifyDao = new Notify_dao();
		//print_r($notifyDao->followUpData($userId)); //die();
		return $notifyDao->followUpData($userId);
		
	}
	
	public function getNotifyList()
	{
		$this->db->where('records.addedById','1');
		$this->db->select('notification.*,records.loanNumber');
		$this->db->from('records');
		$this->db->join('notification','records.id = notification.record_id');
		$query = $this->db->get();
	
		//echo $this->db->last_query(); die();
	
		return  $query->result_array();
	
		//print_r($result); die();
	
	}
	
	public function getRecords($id)
	{
		$this->db->where('records.id',$id);
		$this->db->select('records.*');
		$this->db->from('records');
		$this->db->join('notification','records.id = notification.record_id','left');
		$query = $this->db->get();
	//echo $this->db->last_query(); die();
		return $query->row();
	}
	
	public function getRecords2($id)
	{
		$this->db->where('records.id',$id);
		$this->db->select('notification.comments,notification.receiverName,notification.senderName,notification.creationDate');
		$this->db->from('records');
		$this->db->join('notification','records.id = notification.record_id');
		$query = $this->db->get();
		//echo $this->db->last_query(); die();
		return $query->result();
	}
	
	public function UpdateRecords($id,$data,$dateTime)
	{
		//print_r($data); die();
		
		
	//	print_r($date); die();
		
		if($data['action'] == FOLLOW_UP)
		{
			//$this->db->set('modificationDate','NOW()');
			$this->db->where('id',$id);
			$update = $this->db->update('records',$data);
			if($update){
				$this->db->set('creationDate',date('Y-m-d H:i:s'));
				if($data['comments']!= ''){
				$insert = array('comments'=>$data['comments'],'record_id' => $id,'followupDate'=> date("Y-m-d h:i:s ", strtotime($dateTime['Date'].' '. $dateTime['time'])));
				}
				
				 else {
					$insert = array('record_id' => $id,'followupDate'=>date("Y-m-d h:i:s ", strtotime($dateTime['Date'].' '. $dateTime['time'])));
				} 
				//$this->db->where('record_id',$id);
				$update = $this->db->insert('follow_up',$insert);
				
			//	echo $this->db->last_query(); die();
			}
		}elseif ($data['action'] == NOTIFY){
			$this->db->where('id',$id);
			$update = $this->db->update('records',$data);
			//$insert = $this->db->insert('notifications');
		}else {
			$this->db->where('id',$id);
			$update = $this->db->insert('records',$data);
		}
		
		//echo $this->db->last_query(); die();
		
		if($update)
		{
			return true;
		}
		else {
			return false;
		}
		
		
	}
	
	public function getNotificationRecord($recordId){
		$response = new Response();
		try
		{
			$notifyDao = new Notify_dao();
			$response = $notifyDao->getNotificationRecord($recordId);
		}catch (Exception $e){
			$response->setStatus(false);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
	public function notificationAlert($userId){
		$response = new Response();
		try
		{
			$notifyDao = new Notify_dao();
			$response = $notifyDao->notificationAlert($userId);
		}catch (Exception $e){
			$response->setStatus(false);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
	public function InsertNofity($data,$id,$reciever)
	{
		$notificationData = array(
				'record_id' => $id,
				'comments' => $data['comments'],
				'senderId' => SENDER_ID,
				'senderName' => SENDER_NAME,
				'receiverId' => RECEIVER_ID,
				'receiverName' => RECEIVER_NAME
		);
		
		
		//print_r($data); die();
		$this->db->insert('notification', $notificationData);
	}
	
	public function getLoanManagers()
	{
		$query = $this->db->get('loan_manager');
		$result = $query->result_array();
		return $result;
	}
	
	public function GetCountFollowup()
	{
		$this->db->where('follow_up.flag','0');
		$this->db->where('addedById',SENDER_ID);
		$this->db->like('followupDate',date('Y-m-d'));
		$this->db->select('follow_up.*');
		$this->db->from('follow_up');
		$this->db->join('records','follow_up.record_id = records.id','inner');
		$query = $this->db->get();
	    return $query->num_rows(); //die();
	}
	
 	public function GetCountNotify($id)
	{
		$this->db->where('notification.flag','0');
		$this->db->where('notification.receiverId',$id);
		$this->db->select('notification.*');
		$this->db->from('notification');
		$this->db->join('records','notification.record_id = records.id','inner');
		$query = $this->db->get();
		return $query->num_rows(); //die();
	} 
	/*public function getAllLoanNo($loanNo)
	{		
		$response = new Response();
		try
 		{	
 			$notifyDao = new Notify_dao();
 			$response = $notifyDao->getAllLoanNo($loanNo);
 		}catch (Exception $e){
 			$response->setStatus(false);
 			$response->setMsg($e->getMessage());
 			$response->setError($e->getMessage());
 			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
 		}
 		return $response;
	}*/
	public function getAllLoanNo($loanNo)
	{
		$notifyDao = new Notify_dao();
		$response = $notifyDao->getAllLoanNo($loanNo);
 		return $response;
	}
	
	public function getAllBorrower($name)
	{
		$notifyDao = new Notify_dao();
		$response = $notifyDao->getAllBorrower($name);
 		return $response;
	}
	
	public function getRecords3($id)
	{
		$this->db->where('records.id',$id);
		$this->db->select('follow_up.comments,follow_up.followupDate,follow_up.creationDate');
		$this->db->from('records');
		$this->db->join('follow_up','records.id = follow_up.record_id');
		$query = $this->db->get();
		//echo $this->db->last_query(); die();
		return $query->result();
	}
	
	
	public function GetLastDate($id)
	{
		$this->db->limit(1);
		$this->db->where('records.id',$id);
		$this->db->select('follow_up.followupDate');
		$this->db->from('records');
		$this->db->join('follow_up','records.id = follow_up.record_id');
		$this->db->order_by("follow_up.followupDate", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query(); die();
		return $query->row();
	}
	
	public function GetallNotifications()
	{
		$this->db->select('notification.*,records.loanNumber');
		$this->db->from('notification');
		$this->db->join('records','records.id = notification.record_id');
		$query = $this->db->get();
		return $query->result_array();
		
	}
	
	public function GetFollowUp()
	{
		$this->db->where('records.addedById' ,SENDER_ID );
		$this->db->select('follow_up.comments,follow_up.followupDate');
		$this->db->from('follow_up');
		$this->db->join('records' , 'records.id = follow_up.record_id');
	}
	
	public function Getallfollow()
	{
		$this->db->where('records.addedById',SENDER_ID);
		$this->db->select('follow_up.*,records.loanNumber,records.action');
		$this->db->from('records');
		$this->db->join('follow_up',"records.id = follow_up.record_id and records.action = '1'",'inner');
		$query = $this->db->get();
		
		//echo $this->db->last_query(); die();
		
		return  $query->result_array();
		
		//print_r($result); die();
	}
	
	public function GetCallCategories()
	{
		$query = $this->db->get('call_category');
		$result = $query->result();
		return $result;
		
	}
	
	
	public function getfilterfollowupList($q)
	{
		
		//print_r($q); die();
		
		
			
			/* //echo 123; die();
			 $this->db->where('records.action','1');
			 $this->db->like('follow_up.followupDate',date("Y-m-d"));
			 $this->db->select('follow_up.followupDate');
			 $this->db->select('records.*');
			 $this->db->from('follow_up');
			 $this->db->join('records',"records.id = follow_up.record_id",'inner');
			 $query = $this->db->get(); 
		//	 echo $this->db->last_query(); die();
		//print_r($query->result_array()); die();
			 return $query->result_array();
			  */
		    $sender = SENDER_ID;
			$followup = array();
			$f = array();
			$query = "SELECT distinct records.* FROM records INNER JOIN follow_up on follow_up.record_id = records.id WHERE addedById=".$this->db->escape($sender)." AND action =1 ORDER BY follow_up.followupDate DESC";
			$result = $this->db->query($query);
			if($result->num_rows()>0){
				foreach ($result->result() as $row){
			
					//	print_r($row);
			
					$f['loanNumber'] = $row->loanNumber;
					$f['borrowerName'] = $row->borrowerName;
					$f['borrowerEmail'] = $row->borrowerEmail;
					$f['borrowerPhone'] = $row->borrowerPhone;
					$f['loanOfficer'] = $row->loanOfficer;
					$f['loanAmount'] = $row->loanAmount;
					$f['loanProcessor'] = $row->loanProcessor;
					$f['id'] = $row->id;
					if($q == 1){
						$flag = date('Y-m-d');
						$f['comments']  = $this->getRecordComments($row->id,$flag,$q);
					}elseif ($q == 2){
						$flag = date('Y-m-d');
						$f['comments']  = $this->getRecordComments($row->id,$flag,$q);
					}elseif ($q == 3){
						$flag = date('Y-m-d');
						$f['comments']  = $this->getRecordComments($row->id,$flag,$q);
					}else{
						$flag = "";
						$f['comments']  = $this->getRecordComments($row->id,$flag,$q);
					}
					
					$f['followupDate'] = (sizeof($f['comments'])>0)?$f['comments'][0]['followupDate']:"";
					if(sizeof($f['comments'])>0){
						array_push($followup,$f);
					}
					
				}
					
				//print_r($followup);die();
			}
			return $followup;
			
		
		
		
	}
	
	
	
	public function getRecordComments($recordId,$flag,$q){
		if($q == 1){
			$query = "SELECT * FROM follow_up WHERE record_id=".$this->db->escape($recordId)." AND followupDate LIKE '%".$flag."%' ORDER BY followupDate desc";
			$result = $this->db->query($query);
			return $result->result_array();
		}elseif ($q == 2){ // this week
			$date = strtotime("-7 day");
			$lastdate = date('Y-m-d', $date);
			//die();
			$query = "SELECT * FROM follow_up WHERE record_id=".$this->db->escape($recordId)." AND followupDate between '$lastdate' and '$flag'  ORDER BY followupDate desc"; //print_r($query); die();
			$result = $this->db->query($query);  
			return $result->result_array();
		}elseif ($q == 3){
			$date = strtotime("+7 day");
			$lastdate = date('Y-m-d', $date);
		    $query = "SELECT * FROM follow_up WHERE record_id=".$this->db->escape($recordId)." AND followupDate between '$flag' and '$lastdate' ORDER BY followupDate desc";
			$result = $this->db->query($query);
			return $result->result_array();
		}else{
			$query = "SELECT * FROM follow_up WHERE record_id=".$this->db->escape($recordId)." ORDER BY followupDate desc";
			$result = $this->db->query($query);
			return $result->result_array();
		}
		
	}
	
	public function getfilterNotifyList($q){
		$response =  array();
		$receiver = SENDER_ID ;
		
			$query = "SELECT DISTINCT(record_id) FROM notification  WHERE receiverId=".$this->db->escape($receiver);
			//print_r($query); die();
			$result = $this->db->query($query);
			if($result->num_rows() > 0)
			{
				foreach ($result->result() as $row){
					$n = $this->getNotificationData($receiver, $row->record_id,$q);
				if(sizeof($n['comments']) > 0){
						array_push($response,$n);
					}
				}
				
				//print_r($response);die();
				
			}
		return $response;
	}
	
	
	public function getNotificationData($userId,$recordId,$q)
	{
		$n = array();
		$query = "SELECT N.*,R.loanNumber FROM notification N INNER JOIN records R on R.id = N.record_id WHERE N.receiverId=".$this->db->escape($userId)." ORDER BY N.creationDate DESC LIMIT 1";
		//print_r($query); die();
		$result = $this->db->query($query);
		
		
		
		if($result->num_rows() > 0)
		{
			$n['recordId'] = $recordId;
			$n['loanNumber'] =$result->row()->loanNumber;
			$n['senderId'] = $result->row()->senderId;
			$n['senderName'] = $result->row()->senderName;
			$n['receiverId'] = $result->row()->receiverId;
			$n['receiverName'] =  $result->row()->receiverName;
			$n['creationDate'] =  $result->row()->creationDate; //die();
			
			if($q == 1){
				$flag = date('Y-m-d');
				
				$query = "SELECT * FROM notification notf WHERE notf.record_id=".$this->db->escape($recordId)." AND notf.creationDate LIKE '%".$flag."%' ORDER BY notf.creationDate DESC";
			$result = $this->db->query($query);
			//print_r($query);die();
			$n['comments'] =  $result->result_array();
			}elseif ($q == 2){
				$flag = date('Y-m-d');
				$date = strtotime("-7 day");
				$lastdate = date('Y-m-d', $date);
				$query = "SELECT * FROM notification notf WHERE notf.record_id=".$this->db->escape($recordId)." AND notf.creationDate between '$lastdate' and '$flag' ORDER BY notf.creationDate DESC";
			$result = $this->db->query($query);
		//	print_r($query);die();
			$n['comments'] =  $result->result_array();
			}elseif ($q == 3){
				$flag = date('Y-m-d');
				$date = strtotime("+7 day");
				$lastdate = date('Y-m-d', $date);
				$query = "SELECT * FROM notification notf WHERE notf.record_id=".$this->db->escape($recordId)." AND notf.creationDate between '$flag' and '$lastdate' ORDER BY notf.creationDate DESC";
			$result = $this->db->query($query);
			//print_r($query);die();
			$n['comments'] =  $result->result_array();
			}else{
				$flag = "";
				$query = "SELECT * FROM notification notf WHERE notf.record_id=".$this->db->escape($recordId)." ORDER BY notf.creationDate DESC";
		     	$result = $this->db->query($query);
			//print_r($query);die();
			$n['comments'] =  $result->result_array();
			}
				
			
		}
		return $n;
	}
	
	public function ChangeFlag($id)
	{
		$data['flag'] = '1';
		$this->db->where('record_id',$id);
		$this->db->update('follow_up',$data);
	//	echo $this->db->last_query(); die();
	}
	
	
 }
?>