if (!window.console) { 
    window.console = {
        log: function(obj){ /* define own logging function here, or leave empty */ }
    };
}

/**
*
*
**/
//document.onselectstart=new Function ("return false");
//document.oncontextmenu=new Function("return false");
var isCtrl = false;
var isAlt = false;
var f5 = false;
document.onkeydown=function(e) {
	if(e.which == 116) f5=true;
	/*
	//alert(e.which);
	if(e.which == 17) isCtrl=true;
	if(e.which == 18) isAlt=true;
	if( isCtrl == true ) { isCtrl = false; return false; } // CTRL
	if( isAlt == true ) { isAlt = false; return false; } // ALT
	*/
	if( f5 == true ) { f5 = false; return false; } // F5
}


/**
*
*
**/
function checkUserId(userId) {
	$.ajax({
		type: "GET",
		url: 'ajaxFunction',
		data: 'action=checkUserId&userId='+userId,
		success: function(data) {
			if(data == '0') {
				$("#err_msg").text("Please enter correct userId.");
				$("#userName").html('');
				$("#userId").focus();
				return false;
			} else {
				$("#userName").html(data);
				$("#err_msg").text('');
				return false;
			}
		},
		cache: false
	});
}

/**
*
*
**/
function checkUsername(userId) {
	$.ajax({
		type: "GET",
		url: 'ajaxFunction',
		data: 'action=checkUserId&userId='+userId,
		success: function(data) {
			if(data == '0') {
				$("#err_msg").text("Please enter correct userId.");
				$("#userName").val('');
				$("#userId").focus();
				return false;
			} else {
				$("#userName").val(data);
				$("#err_msg").text('');
				return false;
			}
		},
		cache: false
	});
}

/**
*	change Password page validation
*
**/
function changePasswordValid() {
	new_pass = $("#new_pass").val();
	//old_pass
	if($("#old_pass").val() == "") {
		$("#err_pass").text("Please enter your current Password.");
		$("#old_pass").focus();
		return false;
	} else {
		$("#err_pass").text('');
	}

	if($("#old_pass").val() == $("#new_pass").val()) {
		$("#err_pass").text("Your old password should not be same as your new password.");
		$("#new_pass").focus();
		return false;
	} else {
		$("#err_pass").text('');
	}

	//new_pass
	if(new_pass == "") {
		$("#err_pass").text("Please enter new password.");
		$("#new_pass").focus();
		return false;
	} else {
		$("#err_pass").text('');
	}

	/*
	if($("#new_pass").val().length <8) {
		$("#err_pass").text("Password must be 8 character.");
		$("#new_pass").focus();
		return false;
	} else {
		$("#err_pass").text('');
	}
	*/

	var paswd=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$/;  
	if(!new_pass.match(paswd)) {   
		$("#err_pass").text('Password length should be of 8 to 15 characters. It should contain at least one numeric and one special (!@#$%^&*) character.');
		return false;  
	}

	//conf_pass
	if($("#conf_pass").val() == "") {
		$("#err_pass").text("Please enter confirm password.");
		$("#conf_pass").focus();
		return false;
	} else {
		$("#err_pass").text('');
	}

	// Match new_pass and conf_pass
	if($("#new_pass").val() != $("#conf_pass").val()) {
		$("#err_pass").text("New Password and Confirm password must be same.");
		$("#conf_pass").focus();
		return false;
	}  else {
		$("#err_pass").text('');
	}

}

/**
*
*
**/
function uploadcsv() {
	if($("#loanAssign").val() == "") {
		alert("Please select file");
		return false;
	}
}

/**
*
*
**/
function headerCheck(hId) {
	cond = $("#H_check"+hId).val();
	if(cond != undefined || cond != "" ) {
		if(cond == 'Available') {
			//$("#td"+hId).css('background','#92D050');
			$("#H_check"+hId).css('background','#92D050');
		} else if(cond == 'Not Available') {
			//$("#td"+hId).css('background','#FF0000');
			$("#H_check"+hId).css('background','#FF0000');
		} else if(cond == 'Incomplete') {
			//$("#td"+hId).css('background','#FFFF00');
			$("#H_check"+hId).css('background','#FFFF00');
		} else if(cond == 'Not Applicable') {
			//$("#td"+hId).css('background','#fff');
			$("#H_check"+hId).css('background','#fff');
		} else {
			//$("#td"+hId).css('background','#fff');
			$("#H_check"+hId).css('background','#fff');
		}
	}
	return false;
}

/**
*
*
**/
function changeRemarks(chId) {
	cond = $("#processor_check"+chId).val();
	if(cond != undefined || cond != "" ) {
		if(cond == 'Yes') {
			//$("#td"+chId).css('background','#00B050');
			$("#processor_check"+chId).css('background','#00B050');
		} else if(cond == 'No') {
			//$("#td"+chId).css('background','#FFFF00');
			$("#processor_check"+chId).css('background','#FFFF00');
		} else if(cond == 'N/A') {
			//$("#td"+chId).css('background','#fff');
			$("#processor_check"+chId).css('background','#fff');
		} else {
			//$("#td"+chId).css('background','#fff');
			$("#processor_check"+chId).css('background','#fff');
		}
	}
	return false;
}

/**
*
*
**/
function needList() {
	if(confirm("Are you sure to create need list?")) {
		$("#frmEmpCheckList").submit();
	}
}

/**
* Action after clickling "Go Back" button
* OK : Submit form
* else
* Cancle : Go to previous page
**/
function goback(frmId) {
	if(confirm('Do you want to save these changes?')) {
		$('<input />').attr('type', 'hidden').attr('name', 'goback').attr('value', 'goback').appendTo('#'+frmId);
		$("#"+frmId).submit();
	} else {
		history.back();
	}
}

/**
*
*
**/
function checkListAdd() {
	trbg = '';
	txtxfields = "<tr id='' >";
	txtxfields += "<td align='center'></td>";
	txtxfields += "<td><textarea name='task[]' style='width:99.5%;height:55px;resize:none;'></textarea></td>";
	txtxfields += "<td><textarea name='cp[]' style='width:99.5%;height:55px;resize:none;'></textarea></td>";
	txtxfields += "<td></td>";
	//txtxfields += "<td><textarea name='source[]' style='width:99.5%;height:55px;resize:none;'></textarea></td>";
	txtxfields += "<td><input type='checkbox' name='stage1[]' value='1'>Stage 1<br>";
//	txtxfields += "<input type='checkbox' name='stage2[]' value='1'>Stage 2<br>";
//	txtxfields += "<input type='checkbox' name='client[]' value='1'>Client</td>";
	txtxfields += "</tr>";
	txtxfields += "<tr id='checkListAdd' style='display:none;'><td colspan='4'> &nbsp; </td></tr>";
	$('#checkListAdd').replaceWith(txtxfields);
	return false;
}

/**
*
*
**/
function addCheckListRow(SNo) {
	if(confirm("Are you sure to add row?")) {
		txtxfields = "<tr id='' >";
		txtxfields += "<td align='center'><input type='hidden' name='CHID[]' id='' value=''></td>";
		txtxfields += "<td><textarea name='task[]' style='width:99.5%;height:55px;resize:none;'></textarea></td>";
		txtxfields += "<td><textarea name='cp[]' style='width:99.5%;height:55px;resize:none;'></textarea></td>";
		txtxfields += "<td></td>";
		//txtxfields += "<td><textarea name='source[]' style='width:99.5%;height:55px;resize:none;'></textarea></td>";
		txtxfields += "<td><input type='checkbox' name='stage1[]' value='1'>Stage 1<br>";
	//	txtxfields += "<input type='checkbox' name='stage2[]' value='1'>Stage 2<br>";
	//	txtxfields += "<input type='checkbox' name='client[]' value='1'>Client</td>";
		txtxfields += "</tr>";
		txtxfields += "<tr id='AddCheckListRow"+SNo+"' style='display:none;'><td colspan='3'>&nbsp;</td></tr>";
		$('#AddCheckListRow'+SNo).replaceWith(txtxfields);
		return false;
	}
	return false;
}

/**
*
*
**/
function removeCheckField(chId) {
	if(confirm("Are you sure to delete")) {
		$.ajax({
			type: "GET",
			url: 'ajaxFunction',
			data: 'action=removeCheckField&chId='+chId,
			success: function(msg){
				if(msg == "deleted") {
					location.reload();
				}
		  },
		  cache: false
		});
	}
	return false;
}

/**
*
*
**/
function empCheckListSubmit() {
	loanNo = $("#loanNo").val();
	if($.trim(loanNo) == "") {
		alert("Please enter loan number. ");
		$("#loanNo").focus();
		return false;
	}
}

/**
*
*
**/
function exportStep0(sql) {
	window.location = "ajaxFunction.php?action=exportStep0&sql="+sql;
	return false;
}

/**
*
*
**/
function exportStep1(sql) {
	window.location = "ajaxFunction.php?action=exportStep1&sql="+sql;
	return false;
}

/**
*
*
**/
function exportStep2(sql) {
	window.location = "ajaxFunction.php?action=exportStep2&sql="+sql;
	return false;
}

// ADMIN
/**
*
*
**/
function empValid() {
	var qmsIdLength = $("#qmsId").val().length;
	var nameLength = $("#name").val().length;
	var contactLength = $("#contact").val().length;

	var regionLength = $("#region").val().length;
	var processingManagerLength = $("#processing_manager").val().length;
	var branchLength = $("#branch").val().length;

	if($("#qmsId").val() == "") {
		alert("Please Enter qmsId.");
		$("#qmsId").focus();
		return false;
	} else if (/[^a-zA-Z0-9\ ]/.test($("#qmsId").val())) {
		alert("Please Enter valid qmsId.");
		$("#qmsId").focus();
		return false;
	} else if(qmsIdLength > 10){
		alert("qmsId should be less than ten character.");
		$("#qmsId").focus();
		return false;	
	}

	if($("#name").val() == "") {
		alert("Please enter name.");
		$("#name").focus();
		return false;
	} else if (/[^a-zA-Z0-9\ ]/.test($("#name").val())) {
		alert("Please Enter valid user name.");
		$("#name").focus();
		return false;
	} else if(nameLength > 30){
		alert("Error: Name is too long.");
		$("#qmsId").focus();
		return false;	
	}

	if(isNaN($("#contact").val())) {
		alert("Mobile number is not valid.");
		$("#contact").focus();
		return false;	
	} else if(contactLength > 11) {
		alert("Mobile number should be less than 11 digit.");
		$("#contact").focus();
		return false;
	}


	if($("#password").val() == "") {
		alert("Please enter password.");
		$("#password").focus();
		return false;
	}
	if($("#Cpassword").val() == "") {
		alert("Please enter confirm password.");
		$("#Cpassword").focus();
		return false;
	}
	if($("#password").val() != $("#Cpassword").val()) {
		alert("Password and Confirm password must be same.");
		$("#Cpassword").focus();
		return false;
	}

	var emailCheck = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

	if($("#emailId").val() == "") {
		alert("Please enter email id.");
		$("#emailId").focus();
		return false;
	} else if (!emailCheck.test($("#emailId").val())) {
		alert("Please Enter valid emailId.");
		$("#emailId").focus();
		return false;
	}

	if($("#location").val() == "") {
		alert("Please enter location.");
		$("#location").focus();
		return false;
	}

	if (/[^a-zA-Z0-9\ ]/.test($("#region").val())) {
		alert("Please Enter valid region.");
		$("#region").focus();
		return false;
	} else if(regionLength > 30){
		alert("Region should be less than 30 character.");
		$("#region").focus();
		return false;	
	}

	if(/[^a-zA-Z0-9\ ]/.test($("#processing_manager").val())) {
		alert("Please Enter valid region.");
		$("#processing_manager").focus();
		return false;
	} else if(processingManagerLength > 30) {
		alert("Manager name should be less than 30 character.");
		$("#processing_manager").focus();
		return false;	
	}

	if(/[^a-zA-Z0-9\ ]/.test($("#branch").val())) {
		alert("Please Enter valid branch name.");
		$("#branch").focus();
		return false;
	} else if(branchLength > 30) {
		alert("Branch name should be less than 30 character.");
		$("#branch").focus();
		return false;
	}
}

/**
*
*
**/
function changeStatus(changeIn,id,status) {
	if(changeIn != "" && id != "") {
		if(confirm("Are you sure to change status?")) {
			$.ajax({
				type: "GET",
				url: './ajaxFunction',
				data: 'action=changeStatus&changeIn='+changeIn+'&id='+id+'&status='+status,
				success: function(msg){
					if(msg == "success") {
						//location.reload();
						if(status == "1") {
							$("#statusImg"+id).attr('src','./images/inactive.png');
							$("#statusLink"+id).attr("onClick", "return changeStatus('"+changeIn+"','"+id+"','0');");
						}
						if(status == "0") {
							$("#statusImg"+id).attr('src','./images/active.jpg');
							$("#statusLink"+id).attr("onClick", "return changeStatus('"+changeIn+"','"+id+"','1');");
						}
					} else {
						alert("Unable to change status.");
					}
			  },
			  cache: false
			});
		}
	}
	return false;
}

/**
*
*
**/
function exportNeedList(loan) {
	window.location = "ajaxFunction.php?action=exportNeedList&loan="+loan;
	return false;
}

/**
*
*
***/
function dueBorrowerComment(loanId,table) {
	window.location = "ajaxFunction.php?action=dueBorrowerComment&loanId="+loanId+"&table="+table;
	return false;
}

/**
*
*
***/
function exportReport(sql) {
	window.location = "ajaxFunction.php?action=exportReport&sql="+sql;
	return false;
}

/**
*
*
***/
function completeReportStep2(processor) {
	window.location = "ajaxFunction.php?action=completeReportStep2&processor="+processor;
	return false;
}

/**
*
*
***/
function dfb_step2(report) {
	window.location = "ajaxFunction.php?action=dfb_step2&report="+report;
	return false;
}

/**
*
*
***/
function dfb_step1(report) {
	window.location = "ajaxFunction.php?action=dfb_step1&report="+report;
	return false;
}


/**
*	Create Need List Row on Bottom
*
*/
/*
function needListAdd() {
	sno = $("#rowNo").val();
	txtxfields = "<tr>";
	txtxfields += "<td> <input type='text' name='sno[]' value='' style='width:95%;' class='sno' > </td>";
	txtxfields += "<td> <input type='text' name='status[]' value='' style='width:95%;' class='status' > </td>";
	txtxfields += "<td> <input type='text' name='category[]' value='' style='width:95%;' class='category' > </td>";
	txtxfields += "<td> <input type='text' name='name[]' value='' style='width:95%;' class='name' > </td>";
	txtxfields += "<td> <input type='text' name='type[]' value='' style='width:95%;' class='type' > </td>";
	txtxfields += "<td> <input type='text' name='description[]' value='' style='width:95%;' class='description' > </td>";
	txtxfields += "<td> <input type='text' name='comment[]' value='' style='width:95%;' class='comment' > </td>";
	txtxfields += "<td>";
	txtxfields += "<input type='text' name='lastdate[]' id='lastdate"+sno+"' value='' class='datepicker' style='vertical-align: top;width:57px;' readonly/>";
	txtxfields += "</td>";
	txtxfields += "<td>";
	txtxfields += "<input type='text' name='nextdate[]' id='nextdate"+sno+"' value='' class='datepicker' style='vertical-align: top;width:57px' readonly/>";
	txtxfields += "</td>";
	txtxfields += "</tr>";
	sno = sno+1;
	txtxfields += "<tr id='needListAdd' style='display:none;'><td colspan='9'>";
	txtxfields += "<input type='hidden' name='rowNo' id='rowNo' value='"+sno+"' >";
	txtxfields += "</td> </tr>";
	$('#needListAdd').replaceWith(txtxfields);
	return false;
}
*/

/**
*
*
**/
function backupAssignedloan() {
	window.location = "ajaxFunction.php?action=backupAssignedloan";
	return false;
}

/**
*	Export : Pending loan
*
**/
function pendingLoan(step) {
	window.location = "ajaxFunction.php?action=pendingLoan&step="+step;
	return false;
}

/**
*
*
**/
function eodReport(sql) {
	window.location = "ajaxFunction.php?action=eodReport&sql="+sql;
	return false;
}

/**
*
*
**/
function checklistStep1(loanid) {
	window.location = "ajaxFunction.php?action=checklistStep1&loanid="+loanid;
	return false;
}


/**
*
*
***/
function step1dfb(report) {
	window.location = "ajaxFunction.php?action=step1dfb&report="+report;
	return false;
}

/**
*
*
***/
function step2_dfb_dfp() {
	window.location = "ajaxFunction.php?action=step2_dfb_dfp";
	return false;
}

/**
*
*
**/
function onchangeMailboxStatus(qStatus,lId) {
	if(qStatus != "" && lId != '') {
		if(confirm("Are you sure to change status?")) {
			$.ajax({
				type: "GET",
				url: 'ajaxFunction',
				data: 'action=onchangeMailboxStatus&qStatus='+qStatus+'&lId='+lId,
				success: function(msg){
					if(msg == "Done") {
						alert("Successfully Changed.");
					}
			  },
			  cache: false
			});
		}
	}
	return false;
}

/**
*
*
***/
function freshLoan() {
	window.location = "ajaxFunction.php?action=freshLoan";
	return false;
}


/**
*
*
***/
function notInProcessingInMailbox() {
	window.location = "ajaxFunction.php?action=notInProcessingInMailbox";
	return false;
}

/*
*  Step2 DFB & DFP  report till date
*
**/
function dfb_tillDate_step2(report) {
	window.location = "ajaxFunction.php?action=dfb_tillDate_step2&report="+report;
	return false;
}

function onchangeIndexing(IStatus,qlid) {
	$.ajax({
		type: "GET",
		url: 'ajaxFunction',
		data: 'action=onchangeIndexing&IStatus='+IStatus+'&qlid='+qlid,
		success: function(msg){
			
	  },
	  cache: false
	});
}

function onchangeWStatus(WStatus,qlid) {
	$.ajax({
		type: "GET",
		url: 'ajaxFunction',
		data: 'action=onchangeWStatus&WStatus='+WStatus+'&qlid='+qlid,
		success: function(msg){
			
	  },
	  cache: false
	});
}

function startProcessing(qid) {
	$.ajax({
		type: "GET",
		url: 'ajaxFunction',
		data: 'action=startProcessing&qid='+qid,
		success: function(msg){
			$("#indexing_status"+qid).removeAttr("disabled");
			$("#work_status"+qid).removeAttr("disabled");
			txtxfields = '<input type="button" name="stop'+qid+'" id="stop'+qid+'" value="Stop" onclick="return stopProcessing('+qid+');" >';
			$('#start'+qid).replaceWith(txtxfields);
			return false;
	  },
	  cache: false
	});
	return false;
}

function stopProcessing(qid) {
	$.ajax({
		type: "GET",
		url: 'ajaxFunction',
		data: 'action=stopProcessing&qid='+qid,
		success: function(msg){
			$("#indexing_status"+qid).attr("disabled","disabled");
			$("#work_status"+qid).attr("disabled","disabled");
			txtxfields = '<input type="button" name="start'+qid+'" id="start'+qid+'" value="Start" onclick="return startProcessing('+qid+');" >';
			$('#stop'+qid).replaceWith(txtxfields);
			return false;
	  },
	  cache: false
	});
	return false;
}

/**
*
*
**/
function exportEOD(sql) {
	window.location = "ajaxFunction.php?action=exportEOD&sql="+sql;
	return false;
}

/**
*
*
**/
function reassignloan(loanId) {
	qmsId = $("#qms"+loanId).val();
	searchByQmsId = $("#qmsId").val();
	if($.trim(qmsId) != "") {
		if(confirm("Are you sure to reassign")) {
			$.ajax({
				type: "GET",
				url: 'ajaxFunction',
				data: 'action=reassignloan&qmsId='+qmsId+'&loanId='+loanId+'&searchByQmsId='+searchByQmsId,
				success: function(msg){
					//if(msg == "reassigned") {
					//	location.reload();
					//}
				$("#record").html(msg);
			  },
			  cache: false
			});
		}
	} else {
		alert("Please select Emp Id");
	}
	return false;
}

/**
*
*
**/
function loanReassign() {
	qmsId = $("#reassign_qmsId").val();
	if($.trim(qmsId) == "") {
		alert("Please select reassign to");
		return false;
	}
}

/**
*
*
**/
function exportAssignLoan(sql) {
	window.location = "ajaxFunction.php?action=exportAssignLoan&sql="+sql;
	return false;
}

/**
*
*
**/
function loanInNewTab() {
	loanNo = $("#loanNo").val();
	if(loanNo != '') {
		$("#searchFrm").attr("target","_blank");
	} else {
		$("#searchFrm").removeAttr('target');
	}
}

/**
*
*
**/
function reportReviewMistake() {
	reportReview = $("#reportReview").val();
	if(reportReview != '') {
		$.ajax({
				type: "GET",
				url: 'ajaxFunction',
				data: 'action=reportReviewMistake&reportReview='+reportReview,
				success: function(msg){
					alert(msg);
			  },
			  cache: false
			});
	} else {
		alert("Please enter report review mistake.");
	}
	return false;
}

/**
*
*
**/
function changeuser(usertype, seluser) {
	text = "";
//	if(usertype != "") {
		$.ajax({
			type: "GET",
			url: 'ajaxFunction',
			data: 'action=changeuser&usertype='+usertype,
			success: function(msg){
				var nn = msg.split(" , ");
				text += "<option value='' >All</option>";
				for(i = 0; i < nn.length; i++) {
					if(seluser == nn[i]) { selected = "selected"; } else { selected = ""; }
					text += "<option "+selected+">"+nn[i]+"</option>";
				}
				$("#select_user").html(text);
		  },
		  cache: false
		});
		  /*
	} else {
		text += "<option value=''>Select</option>";
		$("#select_user").html(text);
	}
	*/
	return false
}

/**
*
*
**/
function exporttoexcel(sql) {
	window.location = "ajaxFunction.php?action=exporttoexcel&sql="+sql;
	return false;
}

/**
*
*
**/
function changetype_user_administration(usertype, seluser) {
	text = "";
	$.ajax({
		type: "GET",
		url: 'ajaxFunction',
		data: 'action=changetype_user_administration&usertype='+usertype,
		success: function(msg){
			var nn = msg.split(" , ");
			text += "<option value='' >All</option>";
			for(i = 0; i < nn.length; i++) {
				if(seluser == nn[i]) { selected = "selected"; } else { selected = ""; }
				text += "<option "+selected+">"+nn[i]+"</option>";
			}
			$("#select_user").html(text);
	  },
	  cache: false
	});
	return false
}

/**
*
*
**/
function user_administration_exporttoexcel(sql) {
	window.location = "ajaxFunction.php?action=user_administration_exporttoexcel&sql="+sql;
	return false;
}

/**
*
Added By Abhijeet
*
***/
function exportAllAssignLoan() {
	window.location = "ajaxFunction.php?action=ExportAllAssignLoan";
	return false;
}

/**
*
Added By Abhijeet
*
***/
function exportQueueListLoan(userID) {
	window.location = "ajaxFunction.php?action=ExportQueueListLoan&userID="+userID;
	return false;
}


/**
*
*
**/
function checkListStep3Add() {
	trbg = '';
	txtxfields = "<tr id='' >";
	txtxfields += "<td align='center'></td>";
	txtxfields += "<td><textarea name='task[]' style='width:99.5%;height:55px;resize:none;'></textarea></td>";
	txtxfields += "<td><textarea name='cp[]' style='width:99.5%;height:55px;resize:none;'></textarea></td>";
	txtxfields += "<td> <select name='validate[]'> <option></option> <option>Validate</option> <option>Update</option> </select> </td>";
	txtxfields += "<td> <textarea name='source_doc[]' style='width:99.5%;height:55px;resize:none;'></textarea> </td>";
	txtxfields += "<td> <select name='option_chk[]'> <option></option> <option>Yes</option> <option>No</option> <option>N/A</option> </select> </td>";
	txtxfields += "<td> <textarea name='deficiency[]' style='width:99.5%;height:55px;resize:none;'></textarea> </td>"
	txtxfields += "<td> <select name='creditDecisionDoc[]'> <option></option> <option>REQUIRED</option> </select> </td>";
	txtxfields += "<td><input type='checkbox' name='stage1[]' value='1'>Step 3<br>";
	txtxfields += "</tr>";
	txtxfields += "<tr id='checkListStep3Add' style='display:none;'><td colspan='4'> &nbsp; </td></tr>";
	$('#checkListStep3Add').replaceWith(txtxfields);
	return false;
}

/**
*
*
**/
function addStep3CheckListRow(SNo) {
	if(confirm("Are you sure to add row?")) {
		txtxfields = "<tr id='' >";
		txtxfields += "<td align='center'></td>";
		txtxfields += "<td><textarea name='task[]' style='width:99.5%;height:55px;resize:none;'></textarea></td>";
		txtxfields += "<td><textarea name='cp[]' style='width:99.5%;height:55px;resize:none;'></textarea></td>";
		txtxfields += "<td> <select name='validate[]'> <option></option> <option>Validate</option> <option>Update</option> </select> </td>";
		txtxfields += "<td> <textarea name='source_doc[]' style='width:99.5%;height:55px;resize:none;'></textarea> </td>";
		txtxfields += "<td> <select name='option_chk[]'> <option></option> <option>Yes</option> <option>No</option> <option>N/A</option> </select> </td>";
		txtxfields += "<td> <textarea name='deficiency[]' style='width:99.5%;height:55px;resize:none;'></textarea> </td>"
		txtxfields += "<td> <select name='creditDecisionDoc[]'> <option></option> <option>REQUIRED</option> </select> </td>";
		txtxfields += "<td><input type='checkbox' name='stage1[]' value='1'>Step 3<br>";
		txtxfields += "</tr>";
		txtxfields += "<tr id='addStep3CheckListRow"+SNo+"' style='display:none;'><td colspan='3'>&nbsp;</td></tr>";
		$('#addStep3CheckListRow'+SNo).replaceWith(txtxfields);
		return false;
	}
	return false;
}

/**
*
* export Step3 dashboard
**/
function exportStep3dashboard(sql) {
	window.location = "ajaxFunction.php?action=exportStep3dashboard&sql="+sql;
	return false;
}


/**
* export Step3 doc Order Queue
*
**/
function exportStep3docOrderQueue(sql) {
	window.location = "ajaxFunction.php?action=exportStep3docOrderQueue&sql="+sql;
	return false;
}

function pwdvalid() {
		var oldpwdLength = $("#opwd").val().length;
		var newpwdLength = $("#npwd").val().length;
		var cnpwdLength = $("#cpwd").val().length;


	if($("#opwd").val() == "") {
		alert("Please Enter Password.");
		$("#opwd").focus();
		return false;
	}  else if(oldpwdLength < 8){
		alert("Old Password is not less than 8 characters.");
		$("#opwd").focus();
		return false;	
	}

	if($("#npwd").val() == "") {
		alert("Please Enter Password.");
		$("#npwd").focus();
		return false;
	}  else if(newpwdLength < 8){
		alert("Password should not less than 8 characters.");
		$("#npwd").focus();
		return false;	
	}

	if($("#cpwd").val() == "") {
		alert("Please Enter Password.");
		$("#cpwd").focus();
		return false;
	}  else if(cnpwdLength < 8){
		alert("Password should not less than 8 characters.");
		$("#cpwd").focus();
		return false;	
	}

	if($("#npwd").val() !== $("#cpwd").val()) {
		alert("Password Mismatch");
		return false;
	}
	var verified = '';
	verified = Verify($("#npwd").val());
	verified = Verify($("#cpwd").val());
	function Verify(str) {
    return (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/.test(str));    
}

	if(verified == false) {
		alert("Not a Strong Password: (Uppercase,Lowercase,Digit,Special Character)");
		return false;
	}
	else if(verified == true) {
		return true;
	}

}
