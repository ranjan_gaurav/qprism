var baseUrl = "http://192.168.1.186/qprism/index.php/";

var loanNo = "";
var borrower = "";
var email = "";
var phone = "";
var callCategory = "";
var action = "";
var notifyUser = "";
var notifyUserId ="";
var loanObj = {};

var LO = 1;
var LP = 2;
var PM = 3;
var LO_name = "Jim Moris";
var LP_name = "Ronald";
var PM_name = "Robbinson";

$( document ).ready(function() {
	var date = new Date(); 
 	date.setDate(date.getDate());
	 $('#datetimepicker2').datetimepicker({
		format: 'MM/DD/YYYY', 
 		minDate : date
		// minDate: 0 
     });
	 
	//$('#timepicker2').timepicker();
	 
	 $('#cancel').on('click', function(event) {
		 event.preventDefault();
		 $('#recordForm').get(0).reset();
		 $('#loanForm').hide(500);
	 });
	$('#searchLoan').on('click', function(event) {
		event.preventDefault();
		 var $this = $(this);
		 $(".search-error").html("");
		 loanNo = $.trim($("#loanNo").val());
		 borrower = $.trim($("#borrowerName").val());
		 email = $.trim($("#borrowerEmail").val());
		 phone = $.trim($("#borrowerPhone").val());
		 if(validateSearch( loanNo, borrower, email, phone )){
			 searchLoan( loanNo, borrower, email, phone, $this );
		 }else{
			 $(".search-error").html("Please enter your search criteria.");
		 }
	});
	
	$('#saveData').on('click', function(event) {
		 event.preventDefault();
		 var $this = $(this);
		 
		 //$(".search-error").html("");
		 loanObj.addedById = "1";
		 loanObj.addedByName = "1";
		 loanObj.loanNo = $.trim($("#loanNo").val());
		 loanObj.borrower = $.trim($("#borrowerName").val());
		 loanObj.email = $.trim($("#borrowerEmail").val());
		 loanObj.phone = $.trim($("#borrowerPhone").val());
		 loanObj.loanAmount = $.trim($("#loanAmount").val());
		 loanObj.currentStage = $.trim($("#currentStage").val());
		 loanObj.loanOfficer = $.trim($("#loanOfficer").val());
		 loanObj.loanProcessor = $.trim($("#loanProcessor").val());
		 loanObj.processingManager = $.trim($("#processingManager").val());
		 var callType = [];
         $.each($("input[name='calltype']:checked"), function(){     
        	 callType.push($(this).val());
         });
         console.log(callType);
         if(callType.length>0){
        	 loanObj.callType = callType;
         }else{
        	 loanObj.callType = "";
         }
         loanObj.callCategory = callCategory;
         loanObj.callCategoryNotes = $.trim($("#callCategoryNotes").val());
         loanObj.action = action;
         loanObj.comments = $.trim($("#comment").val());
         if($('#datetimepicker2').data('date')!=undefined){
        	 loanObj.followUpDate = $('#datetimepicker2').data('date');
         }else{
        	 loanObj.followUpDate = "";
         }
		 if($('#datetimepicker3').data('date')!=undefined){
        	 loanObj.followUpTime = $('#datetimepicker3').data('date');
         }else{
        	 loanObj.followUpTime = "";
         }
         loanObj.notifyUser = notifyUser;
         loanObj.notifyUserId = notifyUserId;
		 loanObj.receiver = $.trim($("#select").val());
         console.log(loanObj);
         if(validateForm()){
        	 saveRecords( loanObj, $this );
			   //$('#myModal3').modal('toggle');
         }else{
        	 $(".search-error").html("Please fill all the mandatory fields.");
         }
         
	});
	
	
	
	$('#callCategory').on('change', function() {
		if(this.value!=-1){
			callCategory = this.value;
		}
	});
	$('#notifyUser select').on('change', function() {
		if(this.value!=-1){
			notifyUser = this.value;
			notifyUserId = $(this).children(":selected").attr("id");
		}
	});
	$('#actionable').on('change', function() {
		action = $('input[name="action"]:checked').val();
		if(action==1){ //Follow up
			$('#followUpDate').fadeIn();
			$('#notifyUser').fadeOut();
			$('#followUpTime').fadeIn();
		}else if(action==2){ //Notify
			$('#followUpDate').fadeOut();
			$('#notifyUser').fadeIn();
			$('#followUpTime').fadeOut();
		}else if(action==3){ //No action
			$('#followUpDate').fadeOut();
			$('#notifyUser').fadeOut();
			$('#followUpTime').fadeOut();
		}
	});
	

$( "#loanNo" ).autocomplete({
		  source: function( request, response ) {
			$.ajax( {
			  url: baseUrl+"/Notify/autoSuggestionLoanNo",
			  dataType: "json",
			  data: {
				term: request.term
			  },
			  success: function( data ) {
				 console.log(data)
				response( data );
			  }
			} );
		  },
		  minLength: 1,
		  select: function( event, ui ) {
			//log( "Selected: " + ui.item.value + " aka " + ui.item.id );
		  }
		} );
$( "#borrowerName" ).autocomplete({
		  source: function( request, response ) {
			$.ajax( {
			  url: baseUrl+"Notify/autoSuggestionBorrowerName",
			  dataType: "json",
			  data: {
				term: request.term
			  },
			  success: function( data ) {
				 console.log(data)
				response( data );
			  }
			} );
		  },
		  minLength: 1,
		  select: function( event, ui ) {
			//log( "Selected: " + ui.item.value + " aka " + ui.item.id );
		  }
		} );	
});

function searchLoan( loanNo, borrower, email, phone, $this ){
	var url = baseUrl+"Notify/searchLoan?loanNo="+loanNo+"&borrowerName="+borrower+"&borrowerEmail="+email+"&borrowerPhone="+phone;
	console.log(url);
	$.ajax({
	  url: url,
	  method: "GET",
	  beforeSend: function() {
		//showLoader
		  $this.button('loading');
	  },
	  data: "",
	  dataType: 'json',
      success: function (data) {
		  console.log(data);
		  if(data.status){
			  $(".search-error").html("");
			  $("#loanForm").show(500);
			  var loanData = data.data[0];
			  $("#loanAmount").val(loanData.loan_amt);
			  $("#loanNo").val(loanData.loanNo);
              $("#borrowerName").val(loanData.last_name);
			  $("#currentStage").val(loanData.current_status);
			  $("#loanOfficer").val(loanData.agent);
			  $("#loanProcessor").val(loanData.processor);
			  $("#processingManager").val(loanData.underwriter);
			  $('#select').append('<option value="' + loanData.agent + '">' + loanData.agent + '</option>');
			  $('#select').append('<option value="' + loanData.processor + '">' + loanData.processor + '</option>');
			  $('#select').append('<option value="' + loanData.underwriter + '">' + loanData.underwriter + '</option>');
			  //hideLoader
			  $this.button('reset');
		  }else{
			  $("#loanForm").hide(500);
			  $(".search-error").html("There is no borrower exist in your search criteria.");
			  $("#loanAmount").val("");
			   $("#borrowerName").val("");
			  $("#currentStage").val("");
			  $("#loanOfficer").val("");
			  $("#loanProcessor").val("");
			  $("#processingManager").val("");
			   //hideLoader
			  $this.button('reset');
		  }
	  },
	  error: function (jqXHR, textStatus, errorThrown) {
		  console.log(textStatus);
		  //hideLoader
		    $this.button('reset');
	  }
	});
	
}

function saveRecords( loanObj, $this ){
	var url = baseUrl+"Notify/saveRecords";
	$.ajax({
	  url: url,
	  method: "POST",
	  beforeSend: function() {
		//showLoader
		  $this.button('loading');
	  },
	  data: {loan: loanObj},
	  dataType: 'json',
      success: function (data) {
		  //console.log(data);
		  if(data.status){
			  $(".search-error").html("<span style='color:green'>Data saved successfully.</span>");
			  location.reload(true);
			  $this.button('reset');
		  }else{
			  
			  $(".search-error").html("<span style='color:#f00'>Server error.</span>");
			  $this.button('reset');
		  }
	  },
	  error: function (jqXHR, textStatus, errorThrown) {
		  console.log(jqXHR);
		  console.log(textStatus);
		  console.log(errorThrown);
		  //hideLoader
		    $this.button('reset');
	  }
	});
	
}

function validateSearch( loanNo, borrower, email, phone ){
	if(loanNo!="" || borrower!=""){
		return true;
	}else{
		return false;
	}
}

function validateForm( loanObj ){
	return true;
}


$(document).ready(function(){
		$('.btn-follow-up').on('click', function(){
			$('body').addClass('switch');
			});
		});

		